# SpaceCheckers

This project was undertaken for the course TDT4240 - Software Architecture. The goal is to develop a game for Android and to get familiar with all that follows; Designing, testing and documenting all aspects related to its architecture.

We decided do make our own version of the game known as Chinese Checkers. It's a turn based game where every player has to move his pieces on a board. The first player whose pieces all reach the opposite side wins. 

We believe we achieved this goal as our project works fairly well. Players can join with their custom name, set up their lobby and once all participants are ready, play the game. There is room for improvment; We wanted to add more customization to the playstyle, like adding powerups, more game modes and more boards. Furthermore there are a few bugs that need to be addressed.

<b>Note:</b> Window resizing is not implemented so resizing windows on desktop will offset user click coordinates.

#### Technologies used

The game is developed through Android Studio. Both Java and Kotlin are used as programming languages. To simplify the process, we used the libraries [kryonet](https://github.com/EsotericSoftware/kryonet) to create the Client/Server communication sockets and [libGDX](https://github.com/libgdx/libgdx) to set up the game framework.

#### Content of the repository

The repository includes two Android Studio projects, "CheckersClient" and "CheckersServer". The first contains all the components required to program, build, run and install the client side application, which is the actual game we play on our Android devices. "CheckersServer" allows us to program and set up the server side part of the game instead. 

#### How to configure your server

- Open "CheckersServer" in Android Studio
- By default we use the port 54555 for TCP, to change this go to `core/src/com/mygdx/game/controller/ServerController` and change the line `server.bind(54555);` with the new desired value and save.

#### How to configure the client

To connect to a particular server you need its IP address. Once you have this address, you can set up your client.
- Open "CheckersClient" in Android Studio
- Go to `core/src/com/mygdx/game/controllers/NetworkController` and at the line `String IP4_LAN_ADDRESS = "xxx.xxx.xxx.xxx";` substitute xxx.xxx.xxx.xxx with the server's IP address. The line right below is `client.connect(10000, IP4_LAN_ADDRESS, 54555)`. Here change 54555 with a different TCP port if necessary.

#### How to run the applications

To build an APK in Android Studio click `Build` from the top menu and then select `Build Bundle(s)/Build APK(s)`>`Build APK(s)`. This will produce a package you can use to install the game on your device. 

Otherwise if you don't want to install the game on your personal device you can use Android Studio to run the client and/or server on an emulator by clicking the run button. If you do this, you should launch the server with the desktop launcher by choosing `DesktopLauncher` instead of `android` in the drop down menu next to the build hammer icon. This because emulators have their own private IP address which are not visible outside the emulator.
