package com.mygdx.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.mygdx.game.Constants;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.views.enums.CharacterAssets;
import com.mygdx.game.views.enums.LobbyAssets;
import com.mygdx.game.views.enums.MenuAssets;
import com.mygdx.game.views.tokens.AnimatedSprite;

import java.util.ArrayList;
import java.util.HashMap;

public class MenuView extends AbstractView {

    private final Image connectionBarGreenCircle;
    private final Image connectionBarRedCircle;

    private final Music music;

    private final Sound buttonClickAudio;
    private final Sound transitionAudio;

    private final TextField usernameTextField;

    private final Label outputLabel;

    private final Image avatarPortraitImage;
    private final TextureAtlas portraitAtlas;

    int currentIndexAvatar = 0;

    private final HashMap<Integer, String> availableAvatarsHashMap;

    public MenuView(final GameViewManager gvm, final PlayerController playerController, AssetManager assetManager, final Stage stage, Skin skin) {
        super(gvm, playerController, assetManager, stage, skin);

        for(LobbyAssets asset : LobbyAssets.values()) {
            assetManager.load(asset.path, asset.classType);
        }
        
        availableAvatarsHashMap = Constants.AVAILABLEAVATARSHASHMAP;

        Image backgroundImage = new Image((Texture) assetManager.get(MenuAssets.BACKGROUND.path, MenuAssets.BACKGROUND.classType));
        backgroundImage.setPosition(0, 0);
        backgroundImage.setSize(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

        Image formImage = new Image((Texture) assetManager.get(MenuAssets.FORM.path, MenuAssets.FORM.classType));
        formImage.setPosition(col_width*6- formImage.getWidth()/2, row_height*0.15f);
        formImage.setOrigin(0, 0);

        music = Gdx.audio.newMusic(Gdx.files.internal(MenuAssets.LOOP_AUDIO.path));
        buttonClickAudio = (Sound) assetManager.get(MenuAssets.BUTTON_CLICK_AUDIO.path, MenuAssets.BUTTON_CLICK_AUDIO.classType);
        transitionAudio = (Sound) assetManager.get(MenuAssets.TRANSITION_AUDIO.path, MenuAssets.TRANSITION_AUDIO.classType);

        Image logoImage = new Image((Texture) assetManager.get(MenuAssets.LOGO.path, MenuAssets.LOGO.classType));
        logoImage.setPosition(col_width*6- logoImage.getWidth()/2, row_height*11.75f- logoImage.getHeight());
        logoImage.setOrigin(0,0);

        outputLabel = new Label("", skin,"black");
        outputLabel.setSize(Gdx.graphics.getWidth(),row_height);
        outputLabel.setPosition(col_width*0.5f,row_height*11);
        outputLabel.setFontScale(2);

        usernameTextField = new TextField("", skin);
        usernameTextField.setMessageText("Your name ..");
        usernameTextField.setSize(formImage.getWidth()*0.75f, row_height);
        usernameTextField.setPosition(formImage.getX()+ formImage.getWidth()/2-usernameTextField.getWidth()/2, formImage.getHeight()*0.75f);
        usernameTextField.setOrigin(0,0);

        Button formButton = new TextButton("Enter", skin, "default");
        formButton.setPosition(formImage.getX()+ formImage.getWidth()/2 - formButton.getWidth()/2, formImage.getHeight()*0.08f);

        music.setVolume(1);
        music.setLooping(true);
        music.play();

        Drawable drawable = new TextureRegionDrawable(new TextureRegion(assetManager.get(MenuAssets.NEXT_AVATAR_BUTTON.path, Texture.class)));
        ImageButton nextAvatarButton = new ImageButton(drawable);

        TextureRegion previousAvatarButton = new TextureRegion(assetManager.get(MenuAssets.NEXT_AVATAR_BUTTON.path, Texture.class));
        previousAvatarButton.flip(true,false);
        Drawable flippedDrawable = new TextureRegionDrawable(previousAvatarButton);
        ImageButton previousAvatarButton1 = new ImageButton(flippedDrawable);
        this.portraitAtlas = (TextureAtlas) assetManager.get(CharacterAssets.PortraitsAtlas.path, CharacterAssets.PortraitsAtlas.classType);

        TextureRegion texReggy = portraitAtlas.findRegion(availableAvatarsHashMap.get(currentIndexAvatar)+"Portrait");
        System.out.println("Finding avatar with index:  " + currentIndexAvatar);
        System.out.println("Name is:  " + availableAvatarsHashMap.get(currentIndexAvatar));
        avatarPortraitImage = new Image(texReggy);
        avatarPortraitImage.setBounds(0,0,265,265);
        avatarPortraitImage.setPosition(formImage.getX()+ formImage.getWidth()/2-avatarPortraitImage.getScaleX()*avatarPortraitImage.getWidth()/2, formButton.getY()+ formButton.getHeight()+40);

        nextAvatarButton.setPosition(avatarPortraitImage.getX()+ avatarPortraitImage.getWidth()+col_width*0.8f- nextAvatarButton.getWidth(), avatarPortraitImage.getY()+ avatarPortraitImage.getHeight()/2- nextAvatarButton.getHeight()/2);
        previousAvatarButton1.setPosition(avatarPortraitImage.getX()-col_width*0.8f, avatarPortraitImage.getY()+ avatarPortraitImage.getHeight()/2- previousAvatarButton1.getHeight()/2);

        Image connectionBarImage = new Image(assetManager.get(MenuAssets.CONNECTION_BAR.path, Texture.class));
        connectionBarGreenCircle = new Image(assetManager.get(MenuAssets.GREEN_CIRCLE_SMALL.path, Texture.class));
        connectionBarRedCircle = new Image(assetManager.get(MenuAssets.RED_CIRCLE_SMALL.path, Texture.class));

        connectionBarImage.setPosition(Gdx.graphics.getWidth()- connectionBarImage.getWidth(), Gdx.graphics.getHeight()- connectionBarImage.getHeight());
        connectionBarRedCircle.setPosition(connectionBarImage.getX()+ connectionBarImage.getWidth()*0.75f, connectionBarImage.getY()+ connectionBarImage.getHeight()/2-connectionBarRedCircle.getHeight()/2);
        connectionBarGreenCircle.setPosition(connectionBarImage.getX()+ connectionBarImage.getWidth()*0.75f, connectionBarImage.getY()+ connectionBarImage.getHeight()/2-connectionBarGreenCircle.getHeight()/2);

        previousAvatarButton1.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                if(currentIndexAvatar >0){
                    currentIndexAvatar -= 1;
                    updateAvatarPortrait();
                }
                return true;
            }

        });

        nextAvatarButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                if(availableAvatarsHashMap.size()-1 > currentIndexAvatar){
                    currentIndexAvatar += 1;
                    updateAvatarPortrait();
                }
                return true;
            }
        });

        // Adding listeners
        formButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                if(!usernameTextField.getText().isEmpty() && usernameTextField.getText().length() >=3 && usernameTextField.getText().length() <= 10){
                    buttonClickAudio.play(0.70f);
                    transitionAudio.play(0.50f);
                    playerController.playerSetName(usernameTextField.getText());
                    playerController.updateIndexAvatar(currentIndexAvatar);
                    startFadeout = true;
                }
                else{
                    outputLabel.setText("Please enter a valid name. Has to be between 3 and 10 characters.");
                }
                return true;

            }
        });
        usernameTextField.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                outputLabel.setText("Write a name");
                return true;
            }

            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                buttonClickAudio.play(0.8f);
                if(usernameTextField.getText().isEmpty()){
                    outputLabel.setText(usernameTextField.getText());
                }
            }
        });

        stage.addActor(backgroundImage);
        stage.addActor(connectionBarImage);
        stage.addActor(connectionBarRedCircle);
        stage.addActor(formImage);
        stage.addActor(usernameTextField);
        stage.addActor(formButton);
        stage.addActor(nextAvatarButton);
        stage.addActor(previousAvatarButton1);
        stage.addActor(avatarPortraitImage);
        stage.addActor(logoImage);
        stage.addActor(outputLabel);
    }

    @Override
    public void handleInput(float dt) {
        if (Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
            if(!usernameTextField.getText().isEmpty() && usernameTextField.getText().length() >=3 && usernameTextField.getText().length() <= 10){
                buttonClickAudio.play(0.70f);
                transitionAudio.play(0.50f);
                playerController.playerSetName(usernameTextField.getText());
                playerController.updateIndexAvatar(currentIndexAvatar);
                startFadeout = true;
            }
        }
    }

    private void updateAvatarPortrait(){
       avatarPortraitImage.setDrawable(new TextureRegionDrawable(
               portraitAtlas.findRegion(availableAvatarsHashMap.get(currentIndexAvatar)+"Portrait")
       ));
    }

    @Override
    public void update(float dt) {
        assetManager.update();
        stage.act(dt);

        if(playerController.getPlayer().getID() != -1){
            stage.addActor(connectionBarGreenCircle);
            connectionBarRedCircle.remove();
        }
    }

    @Override
    public void setState() {
        gvm.push(new LobbyView(gvm, playerController, assetManager, stage, skin, availableAvatarsHashMap, music));
    }
}
