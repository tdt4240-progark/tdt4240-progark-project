package com.mygdx.game.views.enums;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public enum CinematicAssets {

    CINEMATIC_AUDIO("cinematic/CinematicMusic.mp3", Music.class),
    COUNT_DOWN_ATLAS("cinematic/atlas/CountDown.atlas", TextureAtlas.class),
    CLOUD_ATLAS("cinematic/atlas/Cloud.atlas", TextureAtlas.class);

    public final String path;
    public final Class classType;

    CinematicAssets(String assetPath, Class classType) {
        this.path = assetPath;
        this.classType = classType;
    }
}