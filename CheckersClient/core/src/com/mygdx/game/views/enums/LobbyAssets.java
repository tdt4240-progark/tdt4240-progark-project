package com.mygdx.game.views.enums;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureArray;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public enum LobbyAssets {

    LOBBY_LIST("Menu/1x/LobbyList.png", Texture.class),
    BACKGROUND("Menu/1x/LobbyMenu@1x.png", Texture.class),
    LOBBY("Menu/1x/Lobby.png", Texture.class),
    MR_SERVER_ATLAS("characters/atlases/MrServer.atlas", TextureAtlas.class),
    MR_SERVER_NAME("characters/MrServerName.png", Texture.class),
    MR_SERVER_BUBBLE_SQUARE("Menu/1x/MrServerBubbleSquare.png", Texture.class),
    MR_SERVER_BUBBLE_POINTER("Menu/1x/MrServerBubblePointer.png", Texture.class),

    ROCKET_BIG("Menu/1x/Rocket_Main@1x.png", Texture.class),
    ROCKET_SMALL("Menu/1x/Rocket_Small@1x.png", Texture.class),
    ROCKET_EXHAUST("Menu/1x/Rocket_Exhaust@1x.png", Texture.class),
    PLAYER_READY_CHECK("Menu/1x/PlayerReadyCheck.png", Texture.class),
    PLAYER_READY("Menu/1x/PlayerReady.png", Texture.class);


    public final String path;
    public final Class classType;

    LobbyAssets(String menuAssetPath, Class classType) {
        this.path = menuAssetPath;
        this.classType = classType;
    }
}
