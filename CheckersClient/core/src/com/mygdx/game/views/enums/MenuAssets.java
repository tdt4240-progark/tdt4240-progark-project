package com.mygdx.game.views.enums;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;

public enum MenuAssets {
    FORM_BUTTON("Menu/1x/FormButton.png", Texture.class),
    BACKGROUND("Menu/1x/MainMenu@1x.png", Texture.class),
    LOGO("Menu/1x/SpaceCheckersLogoWithBackground.png", Texture.class),
    FORM("Menu/1x/MainMenuForm.png", Texture.class),
    LOOP_AUDIO("Menu/MenuLoop.mp3",Sound .class),
    BUTTON_CLICK_AUDIO("Menu/ButtonClick1.mp3",Sound .class),
    TRANSITION_AUDIO("Menu/1.5s_fade.mp3",Sound .class),

    NEXT_AVATAR_BUTTON("Menu/1x/flipButton.png",Texture.class),
    CONNECTION_BAR("Menu/1x/ConnectionBar.png",Texture.class),
    GREEN_CIRCLE_SMALL("Menu/1x/GreenCircleSmall.png",Texture.class),
    RED_CIRCLE_SMALL("Menu/1x/RedCircleSmall.png",Texture.class);


    public final String path;
    public final Class classType;

    MenuAssets(String menuAssetPath, Class classType) {
        this.path = menuAssetPath;
        this.classType = classType;
    }

}

