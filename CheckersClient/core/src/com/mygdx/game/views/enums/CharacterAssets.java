package com.mygdx.game.views.enums;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public enum CharacterAssets {

    AsianGirlAtlas("characters/atlases/AsianGirl.atlas", TextureAtlas.class),
    GrandmaAtlas("characters/atlases/Grandma.atlas", TextureAtlas.class),
    HighSchoolGuyAtlas("characters/atlases/HighSchoolGuy.atlas", TextureAtlas.class),
    HipsterGirlAtlas("characters/atlases/HipsterGirl.atlas", TextureAtlas.class),
    RegularGuyAtlas("characters/atlases/RegularGuy.atlas", TextureAtlas.class),
    RocketGuyAtlas("characters/atlases/RocketGuy.atlas", TextureAtlas.class),
    PortraitsAtlas("characters/atlases/Portraits.atlas", TextureAtlas.class);

    public final String path;
    public final Class classType;

    private CharacterAssets(String menuAssetPath, Class classType) {
        this.path = menuAssetPath;
        this.classType = classType;
    }
}