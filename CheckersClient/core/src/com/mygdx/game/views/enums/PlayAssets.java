package com.mygdx.game.views.enums;

import com.badlogic.gdx.graphics.Texture;

public enum PlayAssets {

    STAR_PIECE_BASE("Game/1x/StarPiece_Base.png", Texture.class),
    STAR_PIECE_BASE_BORDER("Game/1x/StarPiece_BaseBrd.png", Texture.class),
    STAR_PIECE_HEAD("Game/1x/StarPiece_Head.png", Texture.class),
    STAR_PIECE_HEAD_BORDER("Game/1x/StarPiece_HeadBrd.png", Texture.class),
    STAR_PIECE_MAST("Game/1x/StarPiece_Mast.png", Texture.class),
    STAR_PIECE_MAST_BORDER("Game/1x/StarPiece_MastBrd.png", Texture.class),
    ROCKET("Menu/0.25x/Rocket_Main@0.25x.png", Texture.class),
    ROCKET_EXHAUST("Menu/0.25x/Rocket_Exhaust@0.25x.png", Texture.class),
    POSSIBLE_FIELD("Game/1x/Possible_Field.png", Texture.class);

    public final String path;
    public final Class classType;

    PlayAssets(String menuAssetPath, Class classType) {
        this.path = menuAssetPath;
        this.classType = classType;
    }
}
