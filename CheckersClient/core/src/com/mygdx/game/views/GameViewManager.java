package com.mygdx.game.views;


import java.util.Stack;

public class GameViewManager {

    private Stack<AbstractView> views;

    public GameViewManager(){
        views = new Stack<>();
    }

    public void push(AbstractView abstractView){
        views.push(abstractView);
    }

    public void pop(){
        views.pop();
    }

    public void set(AbstractView abstractView){
        views.pop();
        views.push(abstractView);
    }

    public void render(float dt){
        views.peek().render(dt);
    }
}
