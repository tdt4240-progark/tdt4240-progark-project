package com.mygdx.game.views;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.views.enums.CharacterAssets;
import com.mygdx.game.views.enums.MenuAssets;

public class LoadingView extends AbstractView {

    private Image loadingTextImage = new Image(new Texture("Loading/LoadingText.png"));
    private Image loadingDotImage = new Image(new Texture("Loading/LoadingDot.png"));
    private Image loadingBarFillImage = new Image(new Texture("Loading/LoadingBarFill.png"));
    private Image loadingBarBorderImage = new Image(new Texture("Loading/LoadingBarBorder.png"));
    private Image loadingBarBackgroundImage = new Image(new Texture("Loading/LoadingBarBackground.png"));

    public LoadingView(GameViewManager gvm, PlayerController playerController, AssetManager assetManager, Stage stage, Skin skin) {
        super(gvm, playerController, assetManager, stage, skin);

        this.loadingBarBackgroundImage.setPosition((float)(this.col_width * 6) - this.loadingBarBackgroundImage.getWidth() / 2.0F, (float)(this.row_height * 1));
        this.loadingBarBorderImage.setPosition(this.loadingBarBackgroundImage.getX(), this.loadingBarBackgroundImage.getY());
        this.loadingTextImage.setPosition(this.loadingBarBackgroundImage.getX(), this.loadingBarBackgroundImage.getY() + this.loadingTextImage.getHeight() * 1.25F);
        this.loadingDotImage.setPosition(this.loadingTextImage.getX() + this.loadingDotImage.getWidth() * 2.0F, this.loadingTextImage.getY());
        this.loadingBarFillImage.setBounds(this.loadingBarBackgroundImage.getX(), this.loadingBarBackgroundImage.getY(), this.loadingBarBackgroundImage.getWidth() * 0.05F, this.loadingBarBackgroundImage.getHeight());
        stage.addActor(this.loadingTextImage);
        stage.addActor(this.loadingDotImage);
        stage.addActor(this.loadingBarBackgroundImage);
        stage.addActor(this.loadingBarFillImage);
        stage.addActor(this.loadingBarBorderImage);

        for(MenuAssets asset : MenuAssets.values()) {
            assetManager.load(asset.path, asset.classType);
        }

        for(CharacterAssets asset : CharacterAssets.values()) {
            assetManager.load(asset.path, asset.classType);
        }
    }


    @Override
    public void update(float dt) {
        if(assetManager.update()){
            startFadeout = true;
        }
        else{
            loadingBarFillImage.setWidth((assetManager.getProgress()+0.025f)*loadingBarBackgroundImage.getWidth());
        }
    }

    @Override
    public void setState() {
        gvm.set(new MenuView(gvm, playerController, assetManager, stage, skin));
    }
}
