package com.mygdx.game.views;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.game.controllers.GameController;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.DefaultBoard;
import com.mygdx.game.model.Game;
import com.mygdx.game.model.GameMode;
import com.mygdx.game.model.GameState;
import com.mygdx.game.model.gamemodes.rules.DefaultRules;
import com.mygdx.game.views.enums.CharacterAssets;
import com.mygdx.game.views.enums.CinematicAssets;
import com.mygdx.game.views.enums.LobbyAssets;
import com.mygdx.game.views.enums.MenuAssets;
import com.mygdx.game.views.tokens.AnimatedSprite;

import java.util.ArrayList;


public class CinematicView extends AbstractView {

    private final Image lobbyImage;
    private final Image backgroundImage;
    private final Image rocketImage;
    private final Image exhaustImage;
    private final AnimatedSprite countDownAnimationSprite;

    private final ArrayList<AnimatedSprite> lobbyAvatars;
    private final float keyframe1;
    private final float keyframe2;
    private final float keyframe3;
    private final float keyframe4;
    private final Music cinematicSound;
    private final AnimatedSprite cloudSprite;
    private float updateSpritesTimer;
    private float fadeInVolume;
    private float timer = 0;
    private final Camera cam;

    private float cloudSpriteAlpha = 1;

    boolean onlyOnce1 = true;
    boolean onlyOnce2 = true;
    boolean onlyOnce3 = true;

    public CinematicView(GameViewManager gvm, PlayerController playerController, AssetManager assetManager, Stage stage, Skin skin, ArrayList<AnimatedSprite> lobbyAvatars) {
        super(gvm, playerController, assetManager, stage, skin);

        rocketImage = new Image((Texture) assetManager.get(LobbyAssets.ROCKET_SMALL.path, LobbyAssets.ROCKET_SMALL.classType));
        rocketImage.setPosition(1556, 360);

        TextureAtlas countDownAtlas = (TextureAtlas) assetManager.get(CinematicAssets.COUNT_DOWN_ATLAS.path, CinematicAssets.COUNT_DOWN_ATLAS.classType);
        cam = stage.getCamera();
        cam.position.set(rocketImage.getX()+rocketImage.getWidth()/2, cam.viewportHeight/2, 0);

        this.lobbyAvatars = lobbyAvatars;
        fadeInLength = 0.5f;
        fadeOutLength= 0.25f;

        backgroundImage = new Image((Texture) assetManager.get(LobbyAssets.BACKGROUND.path, LobbyAssets.BACKGROUND.classType));

        countDownAnimationSprite = new AnimatedSprite(
                countDownAtlas, new TextureRegion ( (Texture) assetManager.get(MenuAssets.RED_CIRCLE_SMALL.path,
                MenuAssets.RED_CIRCLE_SMALL.classType)),
                "CountDown",
                3);
        countDownAnimationSprite.setPosition(cam.position.x-countDownAnimationSprite.getWidth()/2, rocketImage.getY()+rocketImage.getHeight()/2+countDownAnimationSprite.getHeight()/2);
        countDownAnimationSprite.setAlpha(0);

        exhaustImage = new Image((Texture) assetManager.get(LobbyAssets.ROCKET_EXHAUST.path, LobbyAssets.ROCKET_EXHAUST.classType));
        exhaustImage.setSize(rocketImage.getHeight()/3, rocketImage.getHeight()/2);
        exhaustImage.setPosition(rocketImage.getX()+rocketImage.getWidth()/2-exhaustImage.getWidth()/2, rocketImage.getY()-exhaustImage.getHeight());
        exhaustImage.setColor(1, 1, 1, 0);

        lobbyImage = new Image((Texture) assetManager.get(LobbyAssets.LOBBY.path, LobbyAssets.LOBBY.classType));
        lobbyImage.setPosition(774, 192);

        cinematicSound = (Music) assetManager.get(CinematicAssets.CINEMATIC_AUDIO.path, CinematicAssets.CINEMATIC_AUDIO.classType);

        TextureAtlas cloudAtlas = (TextureAtlas) assetManager.get(CinematicAssets.CLOUD_ATLAS.path, CinematicAssets.CLOUD_ATLAS.classType);
        TextureRegion texReg = cloudAtlas.findRegion("Cloud1");
        cloudSprite = new AnimatedSprite(cloudAtlas, texReg, "Cloud", 8);
        cloudSprite.setPosition(774+lobbyImage.getWidth()/2, 140);
        cloudSprite.setAlpha(0);
        cloudSprite.setOrigin(0, 0);
        countDownAnimationSprite.updateTexture();
        countDownAnimationSprite.updateTexture();

        setFadeColor(Color.BLACK);
        stage.addActor(backgroundImage);
        stage.addActor(lobbyImage);
        stage.addActor(exhaustImage);
        stage.addActor(rocketImage);

        keyframe1 = 1.05f*1.92f;
        keyframe2 = 2.1f*1.92f;
        keyframe3 = 3.15f*1.92f;
        keyframe4 = 3.3f*1.92f;
    }

    @Override
    public void update(float dt) {
        if(!startFadeIn){
            updateSpritesTimer+= dt;
            float updateAvatarFrequency = 0.120f;
            if(0<= timer && timer <= keyframe1){
                for(AnimatedSprite lobbyAvatar : lobbyAvatars){
                    if(updateSpritesTimer >= updateAvatarFrequency) {
                        lobbyAvatar.updateTexture();
                    }
                    lobbyAvatar.translateX(1);
                }
            }
            if(keyframe1 <= timer &&  timer <= keyframe2){
                if(onlyOnce1){
                    cloudSprite.setAlpha(1);
                    countDownAnimationSprite.updateTexture();
                    onlyOnce1 = false;
                }
                for(AnimatedSprite lobbyAvatar : lobbyAvatars){
                    lobbyAvatar.translateX(50);
                    if(lobbyAvatar.getX() >= cloudSprite.getX()+cloudSprite.getWidth()/2){
                        lobbyAvatar.setAlpha( 0);
                    }
                }
            }
            if(keyframe2 <= timer &&  timer <= keyframe3){
                if(onlyOnce2) {
                    countDownAnimationSprite.updateTexture();
                    exhaustImage.setColor(1, 1, 1, 1);
                    onlyOnce2 = false;
                }
                rocketImage.setPosition(rocketImage.getX(), rocketImage.getY()+3);
                exhaustImage.setPosition(exhaustImage.getX(), exhaustImage.getY()+3);
                cam.position.set(rocketImage.getX(), rocketImage.getY(), 0);
            }
            if(keyframe3 <= timer &&  timer <= keyframe4){
                if(onlyOnce3) {
                    countDownAnimationSprite.setAlpha(0);
                    onlyOnce3 = false;
                }
                rocketImage.setPosition(rocketImage.getX(), rocketImage.getY()+50);
                exhaustImage.setPosition(exhaustImage.getX(), exhaustImage.getY()+50);
                cloudSpriteAlpha -= dt*2;
                cloudSprite.setAlpha(cloudSpriteAlpha);
            }
            if(timer>keyframe4){
                startFadeout=true;
                cloudSpriteAlpha -= dt*2;
                cloudSprite.setAlpha(cloudSpriteAlpha);
            }

            if(updateSpritesTimer >= updateAvatarFrequency) {
                cloudSprite.updateTexture();
                updateSpritesTimer = 0;
            }
            timer += dt;
        }
    }

    @Override
    public void setState() {
        // Initialize model and set PlayView
        Game model = new Game(new GameState(new GameMode(new DefaultRules(), new DefaultBoard())), playerController.getLobby().getPlayersID());
        PlayView playView = new PlayView(gvm, playerController, assetManager, stage, skin, lobbyAvatars, model.getBoardImagePath());
        GameController gameController = new GameController(model, playView, playerController);
        playerController.setGameController(gameController);
        playView.setGameController(gameController);
        gvm.set(playView);
    }


    @Override
    public void render(float dt) {
        update(dt);
        stage.draw();

        stage.getBatch().begin();
        for(AnimatedSprite animatedSprite : lobbyAvatars){
            animatedSprite.draw(stage.getBatch());
        }
        countDownAnimationSprite.draw(stage.getBatch());
        cloudSprite.draw(stage.getBatch());
        stage.getBatch().end();

        if(getStartFadeIn() && !getStartFadeOut()) fadeIn(dt);
        else if(getStartFadeOut() && !getStartFadeIn()) fadeOut(dt);
    }

    @Override
    public void fadeIn(float dt) {
        stage.getBatch().begin();
        Color c = stage.getBatch().getColor();
        stage.getBatch().setColor(c.r, c.g, c.b, fadeOutAlpha);
        fadeInAlpha -= dt/fadeInLength;
        stage.getBatch().draw(fadeOverlayTexture, 0, 0, col_width*Help_Guides, row_height*Help_Guides);
        stage.getBatch().setColor(c);
        stage.getBatch().end();

        if(fadeInAlpha <= 0){
            countDownAnimationSprite.setAlpha(1);
            cinematicSound.play();
            startFadeIn = false;

        }
        fadeInVolume += dt/fadeOutLength;
    }

}
