package com.mygdx.game.views;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.game.controllers.PlayerController;

import javax.swing.text.View;


public abstract class AbstractView extends ApplicationAdapter {

    protected OrthographicCamera cam;
    protected float[] lastTouch = new float[2];;
    protected final GameViewManager gvm;
    protected final PlayerController playerController;
    protected final AssetManager assetManager;
    protected final Stage stage;
    protected Skin skin;

    protected final int Help_Guides = 12;
    protected final int row_height = Gdx.graphics.getHeight() / Help_Guides;
    protected final int col_width = Gdx.graphics.getWidth() / Help_Guides;
    protected final float xpad = col_width/2;
    protected final float ypad = row_height/2;

    protected ShapeRenderer shapeRenderer;
    protected float fadeInAlpha;
    protected float fadeOutAlpha;
    protected float fadeInLength;
    protected float fadeOutLength;

    protected boolean startFadeout;
    protected boolean startFadeIn;
    protected Texture fadeOverlayTexture;

    protected AbstractView(GameViewManager gvm, PlayerController playerController, AssetManager assetManager, Stage stage, Skin skin){
        this.gvm = gvm;
        this.stage = stage;
        this.assetManager = assetManager;
        this.skin = skin;
        cam = new OrthographicCamera();
        this.playerController = playerController;
        this.shapeRenderer = new ShapeRenderer();

        fadeInAlpha = 1;
        fadeOutAlpha= 0;
        startFadeIn = true;
        startFadeout = false;
        fadeInLength = 1;
        fadeOutLength = 1;

        Pixmap tablePixmap = new Pixmap(1,1, Pixmap.Format.RGB565);
        tablePixmap.setColor(Color.WHITE);
        tablePixmap.fill();
        fadeOverlayTexture = new Texture(tablePixmap);
    }

    public abstract void update(float dt);
    public abstract void setState();

    // Override when necessary
    protected void handleInput(float dt) {

    }
    protected void render(float dt) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        handleInput(dt);
        update(dt);
        stage.draw();

        if(getStartFadeIn() && !getStartFadeOut()) fadeIn(dt);
        else if(getStartFadeOut() && !getStartFadeIn()) fadeOut(dt);
    }

    protected void fadeIn(float dt) {
        stage.getBatch().begin();
        Color c = Color.WHITE;
        stage.getBatch().setColor(c.r, c.g, c.b, fadeInAlpha);
        fadeInAlpha -= dt/fadeInLength;
        stage.getBatch().draw(fadeOverlayTexture, 0, 0, col_width*Help_Guides, row_height*Help_Guides);
        stage.getBatch().end();

        if(fadeInAlpha <= 0){
            startFadeIn = false;
        }
    }
    protected void fadeOut(float dt) {
        stage.getBatch().begin();
        Color c = Color.WHITE;
        stage.getBatch().setColor(c.r, c.g, c.b, fadeOutAlpha);
        fadeOutAlpha += dt/fadeOutLength;
        //stage.getBatch().draw(fadeOverlayTexture, 0, 0, col_width*Help_Guides, row_height*Help_Guides);
        stage.getBatch().draw(fadeOverlayTexture, cam.position.x/2, cam.position.y/2, col_width*Help_Guides * 2, row_height*Help_Guides * 2);
        stage.getBatch().setColor(c);
        stage.getBatch().end();
        if(fadeOutAlpha >= 1){
            stage.clear();
            startFadeout = false;
            setState();
        }
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    protected void setFadeColor(Color c){
        Pixmap tablePixmap = new Pixmap(1,1, Pixmap.Format.RGB565);
        tablePixmap.setColor(c);
        tablePixmap.fill();
        fadeOverlayTexture = new Texture(tablePixmap);
    }
    public boolean getStartFadeIn(){
        return startFadeIn;
    }
    public boolean getStartFadeOut(){
        return startFadeout;
    }

    protected void drawGrid(){
        if(shapeRenderer == null){
            shapeRenderer = new ShapeRenderer();
        }
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(1, 0, 0, 0.75f); // Red line
        for (int i = 0; i < 12; i++) {
            shapeRenderer.rect(col_width*i, 0, 1, Gdx.graphics.getHeight());
            shapeRenderer.rect(0, row_height*i, Gdx.graphics.getWidth(), 1);
        }
        shapeRenderer.end();
    }
}
