//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.mygdx.game.views.tokens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimatedSprite extends Sprite {

    private final TextureRegion portrait;
    private final String name;
    private final TextureAtlas textureAtlas;
    private int currentFrame = 1;
    private final int maxFrame;
    private boolean reversed = false;


    public AnimatedSprite(TextureAtlas textureAtlas, TextureRegion portrait, String name, int maxFrame) {
        super(textureAtlas.findRegion(name+1));
        this.portrait = portrait;
        this.name = name;
        this.maxFrame = maxFrame;
        this.textureAtlas = textureAtlas;
    }

    public void updateTexture() {
        if (reversed) {
            --currentFrame;
            if (currentFrame == 1) {
                reversed = false;
            }
        } else {
            ++currentFrame;
            if (currentFrame == maxFrame) {
                reversed = true;
            }
        }
        setRegion(textureAtlas.findRegion(name+currentFrame));
    }


    public TextureRegion getPortrait() {
        return this.portrait;
    }

    public String getName() {
        return this.name;
    }
}
