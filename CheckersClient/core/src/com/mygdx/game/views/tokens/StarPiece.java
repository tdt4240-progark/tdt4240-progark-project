package com.mygdx.game.views.tokens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;

public class StarPiece {

    private final Color color;
    private final Texture base;
    private final Texture baseBorder;

    private final Texture mast;
    private final Texture mastBorder;

    private final Texture head;
    private final Texture headBorder;
    private boolean rotateHead;
    private float headRotation = 1;

    Float scale_factor;

    private float baseXPos;
    private float baseYPos;

    private float localHeadXPos;
    private float localHeadYPos;

    private float localMastXPos;
    private float localMastYPos;

    public StarPiece (float scale_factor, float xPos, float yPos, Color color, Texture base, Texture baseBorder, Texture mast, Texture mastBorder, Texture head, Texture headBorder){

        this.color = color;
        this.base = base;
        this.baseBorder = baseBorder;
        this.mast = mast;
        this.mastBorder = mastBorder;
        this.head = head;
        this.headBorder = headBorder;
        this.rotateHead=false;

        this.scale_factor = scale_factor;

        setX(xPos);
        setY(yPos);
    }

    public void draw(Batch sb){
        Color sbColor = sb.getColor();
        sb.begin();
        sb.setColor(color);
        sb.draw(base, baseXPos, baseYPos, base.getWidth() * scale_factor, base.getHeight() * scale_factor);
        sb.setColor(Color.BLACK);
        sb.draw(baseBorder, baseXPos, baseYPos, baseBorder.getWidth() * scale_factor, baseBorder.getHeight() * scale_factor);

        sb.setColor(color);
        sb.draw(mast, localMastXPos, localMastYPos, mast.getWidth() * scale_factor, mast.getHeight() * scale_factor);
        sb.setColor(Color.BLACK);
        sb.draw(mastBorder, localMastXPos, localMastYPos, mast.getWidth() * scale_factor, mast.getHeight() * scale_factor);

        if(rotateHead){
            sb.setColor(color);
            sb.draw(head,
                    localHeadXPos,
                    localHeadYPos,
                    head.getWidth()*scale_factor/2,
                    head.getHeight()*scale_factor/2,
                    head.getWidth()*scale_factor,
                    head.getHeight()*scale_factor,
                    1f,
                    1f,
                    headRotation++,
                    0,
                    0,
                    head.getWidth(),
                    head.getHeight(),
                    false,
                    false);

            sb.setColor(Color.BLACK);
            sb.draw(headBorder,
                    localHeadXPos,
                    localHeadYPos,
                    head.getWidth()*scale_factor/2,
                    head.getHeight()*scale_factor/2,
                    head.getWidth()*scale_factor,
                    head.getHeight()*scale_factor,
                    1f,
                    1f,
                    headRotation++,
                    0,
                    0,
                    head.getWidth(),
                    head.getHeight(),
                    false,
                    false);
        }
        else{
            sb.setColor(color);
            sb.draw(head, localHeadXPos, localHeadYPos, head.getWidth() * scale_factor, head.getHeight() * scale_factor);
            sb.setColor(Color.BLACK);
            sb.draw(headBorder, localHeadXPos, localHeadYPos, head.getWidth() * scale_factor, head.getHeight() * scale_factor);
        }

        sb.setColor(sbColor);
        sb.end();
    }

    public void setRotateHead(boolean rotateHead){
        this.rotateHead = rotateHead;
    }


    public void setX(float xPos){
        this.baseXPos = xPos;
        this.localMastXPos = xPos+ (base.getWidth() * scale_factor)/2-(mast.getWidth() * scale_factor)/2;
        this.localHeadXPos = baseXPos +(base.getWidth() * scale_factor)/2 - (head.getWidth() * scale_factor)/2;
    }

    public void setY(float yPos){
        this.baseYPos = yPos;
        this.localMastYPos = yPos+(base.getHeight() * scale_factor)*0.35f;
        this.localHeadYPos = yPos+(base.getHeight() * scale_factor)-(head.getWidth() * scale_factor)/2;
    }

    public float getX(){
        return baseXPos;
    }

    public float getY(){
        return baseYPos;
    }

    public void setPosition(float xPos, float yPos){
        setX(xPos);
        setY(yPos);
    }
}