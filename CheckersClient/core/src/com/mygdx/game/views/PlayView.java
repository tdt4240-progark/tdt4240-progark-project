package com.mygdx.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Scaling;
import com.mygdx.game.Constants;
import com.mygdx.game.controllers.GameController;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.controllers.UtilsKt;
import com.mygdx.game.model.Player;
import com.mygdx.game.views.enums.PlayAssets;
import com.mygdx.game.views.tokens.AnimatedSprite;
import com.mygdx.game.views.tokens.StarPiece;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class PlayView extends AbstractView {

    int Help_Guides = 12;
    int row_height = Gdx.graphics.getHeight() / Help_Guides;
    int col_width = Gdx.graphics.getWidth() / Help_Guides;

    List<Color> PIECE_COLORS = Arrays.asList(Color.PINK, Color.CYAN, Color.LIME, Color.GOLD, Color.LIGHT_GRAY, Color.PURPLE);
    List<Color> PODIUM_COLORS = Arrays.asList(Color.GOLD, new Color(192/255F, 192/255F, 192/255F, 1), Color.WHITE);

    Texture starPieceBase;
    Texture starPieceBaseBorder;
    Texture starPieceMast;
    Texture starPieceMastBorder;
    Texture starPieceHead;
    Texture starPieceHeadBorder;

    Image boardImage;
    Image backgroundImage;
    Image rocketImage;
    Image rocketExhaustImage;

    ConcurrentHashMap<Vector3, StarPiece> pieces; // Concurrent to avoid ConcurrentModificationException due to render-function
    GameController gameController;
    List<TextField> playerNameFields;

    List<Image> possibleMoves = new ArrayList<>();
    Texture possibleMoveTexture;

    TextButton leaveButton;

    float scale_factor_piece;
    public float hex_side_length;

    public PlayView(GameViewManager gvm, PlayerController playerController, AssetManager assetManager, Stage stage, Skin skin, ArrayList<AnimatedSprite> lobbyAvatars, String boardImagePath) {
        super(gvm, playerController, assetManager, stage, skin);

        stage.getCamera().position.set(Gdx.graphics.getWidth() / 2F, Gdx.graphics.getHeight() / 2F, 0);

        // Create background image
        Texture background = new Texture("Game/1x/StarBackground1x.png");
        backgroundImage = new Image(background);
        backgroundImage.setScaling(Scaling.fill);
        backgroundImage.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage.addActor(backgroundImage);

        // Create board image
        Texture board = new Texture(boardImagePath);
        boardImage = new Image(board);

        // Calculate scale factors for board and pieces, and corresponding hex side length
        float scale_factor_board = Gdx.graphics.getHeight() / boardImage.getHeight();
        this.scale_factor_piece = scale_factor_board * 0.18421F;
        this.hex_side_length = scale_factor_board / 0.0068F;

        boardImage.setScaleX(scale_factor_board);
        boardImage.setScaleY(scale_factor_board);
        boardImage.setPosition(Gdx.graphics.getWidth() / 2F - boardImage.getWidth() * boardImage.getScaleX() / 2F, Gdx.graphics.getHeight() / 2F - board.getHeight() * boardImage.getScaleY() / 2F);
        stage.addActor(boardImage);

        for (PlayAssets asset : PlayAssets.values()) {
            assetManager.load(asset.path, asset.classType);
        }
        assetManager.finishLoading();

        setFadeColor(Color.BLACK);

        // Turn indicator textures (rocket and exhaust)
        Texture rocket = (Texture) assetManager.get(PlayAssets.ROCKET.path, PlayAssets.ROCKET.classType);
        rocketImage = new Image(rocket);
        rocketImage.setScale(0.5F, 0.5F);
        rocketImage.rotateBy(-30);
        stage.addActor(rocketImage);

        Texture rocketExhaust = (Texture) assetManager.get(PlayAssets.ROCKET_EXHAUST.path, PlayAssets.ROCKET_EXHAUST.classType);
        rocketExhaustImage = new Image(rocketExhaust);
        rocketExhaustImage.setScale(0.5F, 0.5F);
        rocketExhaustImage.rotateBy(-30);
        stage.addActor(rocketExhaustImage);

        possibleMoveTexture = (Texture) assetManager.get(PlayAssets.POSSIBLE_FIELD.path, PlayAssets.POSSIBLE_FIELD.classType);

        // StarPiece textures
        starPieceBase = (Texture) assetManager.get(PlayAssets.STAR_PIECE_BASE.path, PlayAssets.STAR_PIECE_BASE.classType);
        starPieceBaseBorder = (Texture) assetManager.get(PlayAssets.STAR_PIECE_BASE_BORDER.path, PlayAssets.STAR_PIECE_BASE_BORDER.classType);
        starPieceMast = (Texture) assetManager.get(PlayAssets.STAR_PIECE_MAST.path, PlayAssets.STAR_PIECE_MAST.classType);
        starPieceMastBorder = (Texture) assetManager.get(PlayAssets.STAR_PIECE_MAST_BORDER.path, PlayAssets.STAR_PIECE_MAST_BORDER.classType);
        starPieceHead = (Texture) assetManager.get(PlayAssets.STAR_PIECE_HEAD.path, PlayAssets.STAR_PIECE_HEAD.classType);
        starPieceHeadBorder = (Texture) assetManager.get(PlayAssets.STAR_PIECE_HEAD_BORDER.path, PlayAssets.STAR_PIECE_HEAD_BORDER.classType);

        placeLeaveButton();

        this.pieces = new ConcurrentHashMap<>();
    }

    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }

    private void isGameOver(){
        boolean someCondition = false;
        if (someCondition){
            Gdx.app.exit();
        }
    }

    @Override
    public void handleInput(float dt) {
        if(Gdx.input.justTouched()) {
            int x = Gdx.input.getX() - (Gdx.graphics.getWidth() / 2);
            int y = Gdx.input.getY() - (Gdx.graphics.getHeight() / 2);

            gameController.handleClick(x, y);
        }
    }

    @Override
    public void update(float dt) {
        stage.act(dt);
        isGameOver();
    }

    @Override
    public void setState() {
        gvm.set(new LobbyView(gvm, playerController, assetManager, stage, skin, Constants.AVAILABLEAVATARSHASHMAP, null));
    }

    @Override
    public void render(float dt) {
        Gdx.gl.glClearColor(1, 1, 1, 1);

        handleInput(dt);
        update(dt);
        stage.draw();
        //stage.draw();

        for (StarPiece piece : this.pieces.values()) {
            piece.draw(stage.getBatch());
        }

        if(getStartFadeIn() && !getStartFadeOut()) fadeIn(dt);
        else if(getStartFadeOut() && !getStartFadeIn()) fadeOut(dt);
    }

    public void initializePieces(List<List<Vector3>> startFieldCubeCoordinates) { // Create and draw pieces at their starting positions
        Color color;
        int colorCounter = 0;

        // Convert to pixel coordinates
        List<List<Float[]>> startFieldPixelCoordinates = UtilsKt.cubeCoordinateSetToPixel(startFieldCubeCoordinates, hex_side_length);

        for (int i = 0; i < startFieldPixelCoordinates.size(); i++) {
            for (int j = 0; j < startFieldPixelCoordinates.get(i).size(); j++) {

                float coordinateX = startFieldPixelCoordinates.get(i).get(j)[0];
                float coordinateY = startFieldPixelCoordinates.get(i).get(j)[1];

                // Base coordinates from center of screen
                coordinateX += Gdx.graphics.getWidth() / 2F - (starPieceBase.getWidth() * scale_factor_piece) / 2F;
                coordinateY = Gdx.graphics.getHeight() / 2F  - coordinateY - (starPieceBase.getHeight() * scale_factor_piece) / 2F + 3F;

                color = PIECE_COLORS.get(colorCounter);
                StarPiece piece = new StarPiece(scale_factor_piece, coordinateX, coordinateY, color, starPieceBase, starPieceBaseBorder, starPieceMast, starPieceMastBorder, starPieceHead, starPieceHeadBorder);
                this.pieces.put(startFieldCubeCoordinates.get(i).get(j), piece);
            }
            colorCounter++;
        }
    }

    public void placePlayerNames(List<Integer> usedBoardSlots, List<List<Float>> coordinateList) {

        List<Player> players = playerController.getLobby().getPlayers();
        playerNameFields = new ArrayList<>();

        for (int i = 0; i < players.size(); i++) {
            TextField textField;

            // Place asterisk in front of client's playername
            if (players.get(i).getID() == playerController.getPlayer().getID()) {
                textField = new TextField("*" + playerController.getPlayer().getPlayerName(), skin);
            } else {
                textField = new TextField(players.get(i).getPlayerName(), skin);
            }
            textField.setColor(PIECE_COLORS.get(i));
            textField.setSize(Constants.PLAYER_NAME_TEXT_FIELD_WIDTH, Constants.PLAYER_NAME_TEXT_FIELD_HEIGHT);
            textField.setPosition(coordinateList.get(usedBoardSlots.get(i)).get(0), coordinateList.get(usedBoardSlots.get(i)).get(1));

            stage.addActor(textField);
            playerNameFields.add(textField);
        }
    }

    public void placeTurnIndicator(List<Float> coordinates) {
        rocketImage.setPosition(coordinates.get(0) - 60F, coordinates.get(1) + 50F);
        rocketExhaustImage.setPosition(rocketImage.getX() - 62F, rocketImage.getY() + 20F - (rocketImage.getHeight()) * rocketImage.getScaleY());
    }

    public void placePossibleMoves(List<Vector3> cubeCoordinates) {
        Float[] coordinates;

        for (Vector3 cubeCoordinate : cubeCoordinates) {
            coordinates = UtilsKt.cubeToPixel(cubeCoordinate, hex_side_length);

            float coordinateX = coordinates[0];
            float coordinateY = coordinates[1];

            Image possibleMoveImage = new Image(possibleMoveTexture);
            possibleMoveImage.setSize(1.2F * hex_side_length, 1.2F * hex_side_length);

            // Base coordinates from center of screen
            coordinateX += Gdx.graphics.getWidth() / 2F - (possibleMoveImage.getWidth() / 2F);
            coordinateY = Gdx.graphics.getHeight() / 2F  - coordinateY - (possibleMoveImage.getHeight() / 2F) + 4F;

            possibleMoveImage.setPosition(coordinateX, coordinateY);
            stage.addActor(possibleMoveImage);
            possibleMoves.add(possibleMoveImage);
        }
    }

    public void removePossibleMoves() {
        for (Image possibleMove : possibleMoves) {
            possibleMove.remove();
        }
    }

    public void setPlayerFinished(int playerIndex, int place) {
        // Weaken playername-field
        TextField playerNameField = playerNameFields.get(playerIndex);
        Color color = playerNameField.getColor();
        color.a = 0.6F;
        playerNameField.setColor(color);

        // Create and place place-field
        TextField placeField;

        if (place >= 0 && place <= 3) {
            List<String> placeStrings= Arrays.asList("st", "nd", "rd");
            placeField = new TextField(place + placeStrings.get(place - 1), skin);
            placeField.setColor(PODIUM_COLORS.get(place - 1));
        } else if (place > 3){
            placeField = new TextField(place + "th", skin);
            placeField.setColor(new Color(205/255F, 127/255F, 50/255F, 1));
        } else {
            placeField = new TextField("dnf", skin);
            placeField.setColor(new Color(205/255F, 127/255F, 50/255F, 1));
        }

        placeField.setSize(115, 65);
        placeField.setPosition(playerNameField.getX() + playerNameField.getWidth() - placeField.getWidth(), playerNameField.getY());

        stage.addActor(placeField);
    }

    public void setGameFinished() {
        // Remove turn indicator and leavebutton. Place exit button
        rocketImage.remove();
        rocketExhaustImage.remove();
        leaveButton.remove();
        placeExitButton();
    }

    public void placeExitButton() {
        TextButton homeButton = new TextButton("Exit", skin, "small");
        homeButton.setPosition(Gdx.graphics.getWidth() / 2F - homeButton.getWidth() / 2F, Gdx.graphics.getHeight() / 2F - homeButton.getHeight() / 2F);

        homeButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                playerController.getPlayer().setIsPlayerReady(false);
                playerController.leaveLobby(playerController.getLobby().getID());
                startFadeout = true;
                return true;
            }
        });

        stage.addActor(homeButton);
    }

    public void placeLeaveButton() {
        leaveButton = new TextButton("Leave", skin, "small");
        leaveButton.setSize(col_width, (float)(row_height*0.75));
        leaveButton.setPosition(Gdx.graphics.getWidth() / 2 + (boardImage.getWidth() * boardImage.getScaleX()) / 3 - leaveButton.getWidth() / 2, Gdx.graphics.getHeight() - 3F * hex_side_length - leaveButton.getHeight() / 2F);

        leaveButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                playerController.getPlayer().setIsPlayerReady(false);
                playerController.leaveLobby(playerController.getLobby().getID());
                startFadeout = true;
                return true;
            }
        });

        stage.addActor(leaveButton);
    }

    public StarPiece getPiece(Vector3 coordinates) {
        return this.pieces.get(coordinates);
    }

    public void movePiece(StarPiece piece, Vector3 toCoordinates) {
        Float[] pixelCoordinates = UtilsKt.cubeToPixel(toCoordinates, hex_side_length);

        piece.setX((Gdx.graphics.getWidth() / 2F) + pixelCoordinates[0] - ((starPieceBase.getWidth() * scale_factor_piece) / 2F));
        piece.setY(Gdx.graphics.getHeight() / 2F - pixelCoordinates[1] - ((starPieceBase.getHeight() * scale_factor_piece) / 2F));

        // Replace key
        pieces.values().remove(piece);
        pieces.put(toCoordinates, piece);
    }

    public void moveOtherPlayerPiece(Vector3 fromCoordinates, Vector3 toCoordinates) {
        Float[] pixelCoordinates = UtilsKt.cubeToPixel(toCoordinates, hex_side_length);

        StarPiece piece = getPiece(fromCoordinates);
        piece.setX((Gdx.graphics.getWidth() / 2F) + pixelCoordinates[0] - ((starPieceBase.getWidth() * scale_factor_piece) / 2F));
        piece.setY(Gdx.graphics.getHeight() / 2F - pixelCoordinates[1] - ((starPieceBase.getHeight() * scale_factor_piece) / 2F));

        // Replace key
        pieces.put(toCoordinates, pieces.remove(fromCoordinates));
    }

    public void removePieces(List<Vector3> coordinates) {
        for (Vector3 coordinate : coordinates) {
            pieces.remove(coordinate);
        }
    }
}
