package com.mygdx.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Align;

import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.Lobby;
import com.mygdx.game.model.Player;
import com.mygdx.game.views.enums.CharacterAssets;
import com.mygdx.game.views.enums.CinematicAssets;
import com.mygdx.game.views.enums.LobbyAssets;
import com.mygdx.game.views.enums.MenuAssets;
import com.mygdx.game.views.tokens.AnimatedSprite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class LobbyView extends AbstractView {

    private final ArrayList<Actor> playerUIWidgets = new ArrayList<>();
    private final Music currentSong;
    private final TextureAtlas portraitAtlas;

    private final Button createLobbyButton;
    private final Button findLobbyButton;
    private final Button joinLobbyButton;
    private final Button lobbyBackButton;
    private final Button lobbyListRefreshButton;
    private final Button lobbyRefreshButton;
    private final Button lobbyListBackButton;
    private final Button startLobbyButton;

    private float updateAvatarTimer = 0;
    private float refreshLobbyTimer = 1;

    private final Image backgroundImage;
    private final AnimatedSprite mrServer;
    private final Image lobbyListBubbleImage;

    private final Label mrServerBubbleLabel;
    private final Label outputLabel;

    private final Sound buttonClickAudio;

    private final TextField lobbyCreateTextField;

    private final Camera camera;

    private final ImageButton playerReadyCheckButton;

    private final SelectBox<Integer> lobbyCreateSizeSelectBox;

    private ScrollPane scrollPane;
    private List<String> lobbyListStrings;
    private HashMap<String, Integer> getLobbyByStringHashMap;

    private final Image lobbyImage;

    private final ArrayList<AnimatedSprite> lobbyAvatars = new ArrayList<>();

    private final ArrayList<Image> playerReadyImages = new ArrayList<>();
    private final ArrayList<Label> lobbyLabels= new ArrayList<>();
    private final HashMap<Integer, String> availableAvatarsHashMap;

    public LobbyView(GameViewManager gvm, final PlayerController playerController, AssetManager assetManager, final Stage stage, Skin skin, HashMap<Integer,String> availableAvatarsHashMap, Music currentSong) {
        super(gvm, playerController, assetManager, stage, skin);

        this.portraitAtlas = (TextureAtlas) assetManager.get(CharacterAssets.PortraitsAtlas.path, CharacterAssets.PortraitsAtlas.classType);

        if (currentSong == null) {
            this.currentSong = Gdx.audio.newMusic(Gdx.files.internal(MenuAssets.LOOP_AUDIO.path));
            this.currentSong.setVolume(1);
            this.currentSong.setLooping(true);
            this.currentSong.play();
        } else {
            this.currentSong = currentSong;
        }

        buttonClickAudio = (Sound) assetManager.get(MenuAssets.BUTTON_CLICK_AUDIO.path, MenuAssets.BUTTON_CLICK_AUDIO.classType);
        this.camera = stage.getCamera();

        this.availableAvatarsHashMap = availableAvatarsHashMap;

        for(CinematicAssets asset : CinematicAssets.values()) {
            assetManager.load(asset.path, asset.classType);
        }

        Image playerReadyCheck = new Image(assetManager.get(LobbyAssets.PLAYER_READY_CHECK.path, Texture.class));
        playerReadyCheckButton = new ImageButton(playerReadyCheck.getDrawable());
        playerReadyCheckButton.setPosition(col_width*6, row_height*2);

        backgroundImage = new Image((Texture) assetManager.get(LobbyAssets.BACKGROUND.path, LobbyAssets.BACKGROUND.classType));
        stage.getViewport().setWorldSize(backgroundImage.getWidth(), backgroundImage.getHeight());

        Image rocketImage = new Image((Texture) assetManager.get(LobbyAssets.ROCKET_SMALL.path, LobbyAssets.ROCKET_SMALL.classType));
        rocketImage.setPosition(1556, 360);


        Image exhaustImage = new Image((Texture) assetManager.get(LobbyAssets.ROCKET_EXHAUST.path, LobbyAssets.ROCKET_EXHAUST.classType));
        exhaustImage.setSize(rocketImage.getHeight()/6, rocketImage.getHeight()/2);
        exhaustImage.setPosition(rocketImage.getX()+ rocketImage.getWidth()/2- exhaustImage.getWidth()/2, rocketImage.getY()- exhaustImage.getHeight());
        exhaustImage.setColor(1, 1, 1, 0);


        TextureAtlas portraitAtlas = (TextureAtlas)assetManager.get(CharacterAssets.PortraitsAtlas.path, CharacterAssets.PortraitsAtlas.classType);
        mrServer = new AnimatedSprite( (TextureAtlas) assetManager.get(LobbyAssets.MR_SERVER_ATLAS.path, LobbyAssets.MR_SERVER_ATLAS.classType),
                portraitAtlas.findRegion("MrServerPortrait"),
                "MrServer",
                3);
        mrServer.scale(-0.63f);
        mrServer.setOrigin(0,0 );
        mrServer.setOriginBasedPosition(190, 1);

        Image mrServerNameImage = new Image((Texture) assetManager.get(LobbyAssets.MR_SERVER_NAME.path, LobbyAssets.MR_SERVER_NAME.classType));
        mrServerNameImage.setPosition(180+ mrServerNameImage.getWidth()/2, 360);

        lobbyImage = new Image((Texture) assetManager.get(LobbyAssets.LOBBY.path, LobbyAssets.LOBBY.classType));
        lobbyImage.setPosition(774, 192);

        lobbyListBubbleImage = new Image((Texture) assetManager.get(LobbyAssets.MR_SERVER_BUBBLE_SQUARE.path, LobbyAssets.MR_SERVER_BUBBLE_SQUARE.classType));
        lobbyListBubbleImage.setPosition(29.535f, 4328-3930);

        Image lobbyListBubblePointerImage = new Image((Texture) assetManager.get(LobbyAssets.MR_SERVER_BUBBLE_POINTER.path, LobbyAssets.MR_SERVER_BUBBLE_POINTER.classType));
        lobbyListBubblePointerImage.setPosition(lobbyListBubbleImage.getX()+ lobbyListBubblePointerImage.getWidth()*1.25f, lobbyListBubbleImage.getY()- lobbyListBubblePointerImage.getHeight()+2);

        outputLabel = new Label("",skin,"black");
        outputLabel.setSize(Gdx.graphics.getWidth(),row_height);
        outputLabel.setPosition(col_width*0.25f,row_height*11);
        outputLabel.setAlignment(Align.topLeft);
        outputLabel.setFontScale(2);

        createLobbyButton = new TextButton("Create Lobby",skin,"small");
        createLobbyButton.setSize(col_width*1f,(float)(row_height*0.75));
        createLobbyButton.setPosition(lobbyListBubbleImage.getX()+xpad,lobbyListBubbleImage.getY()+ypad);

        findLobbyButton = new TextButton("Find Lobby",skin,"small");
        findLobbyButton.setSize(col_width*1f,(float)(row_height*0.75));
        findLobbyButton.setPosition(lobbyListBubbleImage.getX()+lobbyListBubbleImage.getWidth()-findLobbyButton.getWidth()-xpad,lobbyListBubbleImage.getY()+ypad);

        mrServerBubbleLabel = new Label("Hi, I am mr. Server. I allow creation and joining of lobby rooms.", skin,"black");
        mrServerBubbleLabel.setSize(lobbyListBubbleImage.getWidth()-xpad*2, lobbyListBubbleImage.getHeight()-createLobbyButton.getY()-createLobbyButton.getHeight()-ypad*2);
        mrServerBubbleLabel.setPosition(lobbyListBubbleImage.getX()+xpad, createLobbyButton.getY()+lobbyListBubbleImage.getHeight()-ypad*3);

        mrServerBubbleLabel.setOrigin(0,0);
        mrServerBubbleLabel.setWrap(true);
        mrServerBubbleLabel.setFontScale(2);

        lobbyListRefreshButton = new TextButton("Refresh",skin,"small");
        lobbyListRefreshButton.setSize(col_width*1f,(float)(row_height*0.65));
        lobbyListRefreshButton.setPosition(createLobbyButton.getX(), createLobbyButton.getY());

        lobbyRefreshButton = new TextButton("Refresh",skin,"small");
        lobbyRefreshButton.setSize(col_width*1f,(float)(row_height*0.65));
        lobbyRefreshButton.setPosition(createLobbyButton.getX(), createLobbyButton.getY());

        lobbyBackButton = new TextButton("Back",skin,"small");
        lobbyBackButton.setSize(col_width*1f,(float)(row_height*0.65));
        lobbyBackButton.setPosition(findLobbyButton.getX(), findLobbyButton.getY());

        lobbyListBackButton = new TextButton("Back",skin,"small");
        lobbyListBackButton.setSize(col_width*1f,(float)(row_height*0.65));
        lobbyListBackButton.setPosition(findLobbyButton.getX(),findLobbyButton.getY());

        joinLobbyButton = new TextButton("Join Lobby",skin,"small");
        joinLobbyButton.setSize(col_width*1f,(float)(row_height*0.65));
        joinLobbyButton.setPosition(findLobbyButton.getX(), findLobbyButton.getY()+ypad+findLobbyButton.getHeight()/3);

        startLobbyButton = new TextButton("Start",skin,"small");
        startLobbyButton.setSize(col_width*1f,(float)(row_height*0.65));
        startLobbyButton.setPosition(createLobbyButton.getX(), createLobbyButton.getY());

        lobbyCreateTextField = new TextField("", skin);
        lobbyCreateTextField.setMessageText("Enter lobby name.");
        lobbyCreateTextField.setSize(col_width*1.5f,row_height*1f);
        lobbyCreateTextField.setPosition(lobbyListRefreshButton.getX(), lobbyListRefreshButton.getY()+row_height*1.25f);

        lobbyCreateSizeSelectBox =new SelectBox<>(skin);
        lobbyCreateSizeSelectBox.setItems(2, 4, 6);
        lobbyCreateSizeSelectBox.setSize(lobbyListBackButton.getWidth() / 2, lobbyListBackButton.getHeight());
        lobbyCreateSizeSelectBox.setPosition(lobbyListBackButton.getX() + lobbyCreateSizeSelectBox.getWidth() / 2, lobbyCreateTextField.getY()+lobbyCreateTextField.getHeight()/2-lobbyCreateSizeSelectBox.getHeight()/2);

        backgroundImage.setZIndex(1);
        backgroundImage.setOrigin(0,0);

        stage.addActor(backgroundImage);
        stage.addActor(rocketImage);
        stage.addActor(exhaustImage);
        stage.addActor(lobbyImage);
        stage.addActor(lobbyListBubbleImage);
        stage.addActor(lobbyListBubblePointerImage);
        stage.addActor(mrServerNameImage);

        playerUIWidgets.add(createLobbyButton);
        playerUIWidgets.add(findLobbyButton);
        playerUIWidgets.add(mrServerBubbleLabel);
        for (Actor widget: playerUIWidgets){
            stage.addActor(widget);
        }
        stage.addActor(outputLabel);

        createLobbyButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                showCreateLobbyWindow();
            }
        });
        playerReadyCheckButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                playerController.setLobbyPlayerReady(true);
            }
        });

        findLobbyButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                showLobbyListWindow();
            }
        });
        lobbyListBackButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                Gdx.input.setOnscreenKeyboardVisible(false);
                stage.unfocusAll();
                Lobby lobby = playerController.getLobby();
                try{
                    if(lobby.getPlayers().size() == 1){
                        playerController.deleteLobby(lobby.getID());
                    }
                    else{
                        playerController.leaveLobby(lobby.getID());
                    }

                }catch(NullPointerException e){

                }
                showMainLobbyWindow();
            }
        });
        lobbyListRefreshButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                outputLabel.setText("Lobby refreshed!");
                lobbyListRefresh();
            }
        });

        lobbyBackButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                Gdx.input.setOnscreenKeyboardVisible(false);
                stage.unfocusAll();

                Lobby lobby = playerController.getLobby();

                if(lobby.getID() == -1){System.out.println("Currently not in a lobby.");}

                if(lobby.getPlayers().size() == 1){
                    System.out.println("Deleting lobby " + lobby.getName());
                    playerController.setLobbyPlayerReady(false);
                    playerController.deleteLobby(lobby.getID());
                    playerController.getPlayer().setIsPlayerReady(false);
                }
                else{
                    System.out.println("Leaving lobby " + lobby.getName());
                    playerController.setLobbyPlayerReady(false);
                    playerController.leaveLobby(lobby.getID());
                    playerController.getPlayer().setIsPlayerReady(false);
                }

                for (Label label :lobbyLabels) label.remove();
                for(Image image: playerReadyImages) image.remove();
                playerReadyCheckButton.remove();
                lobbyAvatars.clear();
                showMainLobbyWindow();

            }
        });

        joinLobbyButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {

                if(getLobbyByStringHashMap.containsKey(lobbyListStrings.getSelected())){
                    Thread thread = new Thread(){
                        public void run(){
                            for(Lobby lobby : playerController.getLobbyList()){
                                System.out.println("LobbyId available: " +lobby.getID());
                            }
                            playerController.joinLobby(getLobbyByStringHashMap.get(lobbyListStrings.getSelected()));
                            System.out.println("joining lobby with lobbyID: " + lobbyListStrings.getSelectedIndex());
                            while (playerController.getLastCommand() == null) {
                                System.out.println("Waiting for lobby");
                            }
                            Lobby lobby = playerController.getLobby();
                            System.out.println("Lobby loaded " + lobby);
                            Gdx.input.setOnscreenKeyboardVisible(false);
                            showLobbyWindow();
                            lobbyRefresh();
                        }
                    };
                    thread.start();
                }
            }
        });

        lobbyRefreshButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
                outputLabel.setText("Lobby refreshed!");
                lobbyRefresh();
            }
        });

        lobbyCreateTextField.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.8f);
            }

            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                buttonClickAudio.play(0.8f);
            }
        });
        startLobbyButton.addListener(new InputListener(){
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                buttonClickAudio.play(0.80f);
                if(!lobbyCreateTextField.getText().isEmpty()){
                    Thread thread = new Thread(){
                        public void run(){
                            playerController.createLobby(lobbyCreateTextField.getText(), lobbyCreateSizeSelectBox.getSelected());
                            System.out.println("Creating lobby '" + lobbyCreateTextField.getText() + "' with maxSize " + lobbyCreateSizeSelectBox.getSelected());
                            while (playerController.getLastCommand() == null) {
                                System.out.println("Waiting for lobby object from server..");
                            }
                            while(playerController.getLobby().getID() == -1){
                                System.out.println("Waiting for lobby object from server. Current id is -1..");
                            }

                            Lobby lobby = playerController.getLobby();
                            System.out.println("Lobby loaded successfully.  '" + playerController.getLobby().getName() + "', maxPlayers: " + lobby.getMaxPlayers() + ", NumOfPlayersInLobby: " + lobby.getPlayers().size());

                            playerController.joinLobby(playerController.getLobby().getID());
                            while(playerController.getLastCommand() == null){System.out.println("NOT JOINED LOBBY YET");}
                            Gdx.input.setOnscreenKeyboardVisible(false);
                            showLobbyWindow();
                            lobbyRefresh();
                        }
                    };
                    thread.start();
                }
                return true;
            }
        });
    }

    private void clearUIWidgets(){
        for (Actor widget: playerUIWidgets){
            widget.remove();
        }
        playerUIWidgets.clear();
    }

    public void lobbyListRefresh(){
        Thread thread = new Thread(){
            public void run(){
                playerController.setLobbies(null);
                playerController.lobbyGetList();
                while (playerController.getLastCommand() == null) {
                    System.out.println("Waiting for lobby list .. ");
                }
                ArrayList<Lobby> lobbyList = playerController.getLobbyList();
                System.out.println("Lobby loaded " + lobbyList + " Number of available lobbies are " + lobbyList.size());
                System.out.println("LobbyCount AFTER" + lobbyList.size());

                lobbyListStrings = new List(skin, "plain");
                lobbyListStrings.setColor(Color.BLACK);
                String[] strings;

                if(getLobbyByStringHashMap == null) getLobbyByStringHashMap = new HashMap<>();
                else getLobbyByStringHashMap.clear();

                if(lobbyList.size() > 0){
                    strings = new String[lobbyList.size()];
                    for (int i = 0; i < lobbyList.size(); i++) {
                        strings[i] = lobbyList.get(i).getName() + "\t" + lobbyList.get(i).getPlayers().size()+ "/" + lobbyList.get(i).getMaxPlayers();
                        getLobbyByStringHashMap.put(strings[i], lobbyList.get(i).getID());
                    }
                }
                else{
                    strings = new String[1];
                    strings[0] = "No lobbies found.";
                }

                if(scrollPane != null) scrollPane.remove();

                playerUIWidgets.remove(scrollPane);

                lobbyListStrings.setItems(strings);
                scrollPane = new ScrollPane(lobbyListStrings);
                scrollPane.setBounds(0, 0, col_width*4f-xpad, row_height*3f);
                scrollPane.setPosition(col_width*0.52f, row_height*6.2f);
                scrollPane.setSmoothScrolling(false);
                scrollPane.setTransform(true);


                playerUIWidgets.add(scrollPane);

                for (Actor actor: playerUIWidgets){
                    stage.addActor(actor);
                }
            }
        };
        thread.start();
    }
    private void showCreateLobbyWindow(){
        clearUIWidgets();
        playerUIWidgets.add(lobbyListBackButton);
        playerUIWidgets.add(lobbyCreateTextField);
        playerUIWidgets.add(lobbyCreateSizeSelectBox);
        playerUIWidgets.add(startLobbyButton);
        lobbyListBackButton.setY(startLobbyButton.getY());
        for (Actor actor: playerUIWidgets){
            stage.addActor(actor);
        }
    }
    private void showLobbyListWindow(){
        clearUIWidgets();
        playerUIWidgets.add(lobbyListBackButton);
        playerUIWidgets.add(lobbyListRefreshButton);
        playerUIWidgets.add(joinLobbyButton);

        for (Actor actor: playerUIWidgets){
            stage.addActor(actor);
        }
        lobbyListRefresh();
    }

    private void lobbyRefresh(){
        if(playerController.getLobby().getID() == -1) {
            // Player is currently not in a lobby.
        }
        else{
            lobbyAvatars.clear();
            Lobby lobby = playerController.getLobby();

            if(lobby.getID() == -1 || playerController.getPlayer().getIsPlayerReady()) playerReadyCheckButton.remove();
            else stage.addActor(playerReadyCheckButton);

            for (Label label : lobbyLabels) label.remove();
            for (Image image : playerReadyImages) image.remove();
            playerReadyImages.clear();

            for(int i = 0; i<lobby.getPlayers().size(); i++){
                Player player = lobby.getPlayers().get(i);

                String path = Enum.valueOf(CharacterAssets.class, availableAvatarsHashMap.get(player.getIndexAvatar())+"Atlas").path;
                Class classType = Enum.valueOf(CharacterAssets.class, availableAvatarsHashMap.get(player.getIndexAvatar())+"Atlas").classType;

                AnimatedSprite animatedSprite = new AnimatedSprite(
                        (TextureAtlas) assetManager.get(path, classType),
                        portraitAtlas.findRegion(availableAvatarsHashMap.get(player.getIndexAvatar())+"Portrait"),
                        availableAvatarsHashMap.get(player.getIndexAvatar()),
                        3);

                animatedSprite.setOrigin(0, 0);
                int remY = mod(2, i+2);
                int xPad = 25;

                animatedSprite.setPosition(lobbyImage.getX()-xPad+i*((lobbyImage.getWidth()-xPad)/4.5F), lobbyImage.getY()-remY*(lobbyImage.getHeight()/7.5f));
                animatedSprite.setScale(0.3f+0.025f*remY, 0.3f+0.025f*remY);
                lobbyAvatars.add(animatedSprite);

                Label label = new Label("Player"+i+":\t" + player.getPlayerName() + "\t", skin, "black");
                label.setColor(Color.BLACK);
                label.scaleBy(2f);
                label.setPosition(lobbyListBubbleImage.getX()+xpad, lobbyListBubbleImage.getY()+lobbyListBubbleImage.getHeight() - (i + 1) * (label.getHeight() * 2.5F));

                stage.addActor(label);
                lobbyLabels.add(label);

                if(player.getIsPlayerReady()) {
                    Image image = new Image(assetManager.get(LobbyAssets.PLAYER_READY.path, Texture.class));
                    image.setPosition(label.getX()+image.getWidth()+col_width*1f, label.getY()-image.getHeight()/2+label.getHeight()/2);
                    playerReadyImages.add(image);
                }
                else{
                    // Player is not ready
                }
            }
            for (Image image : playerReadyImages){
                stage.addActor(image);
            }
        }
    }

    private int mod(int a, int b)
    {
        int c = (a % b + b) % b;
        if(c == a){
            return 0;
        }
        else{
            return c;
        }
    }

    private void showLobbyWindow(){
        clearUIWidgets();
        playerUIWidgets.add(lobbyBackButton);
        playerUIWidgets.add(lobbyRefreshButton);

        for (Actor actor: playerUIWidgets){
            stage.addActor(actor);
        }
    }

    private void showMainLobbyWindow(){
        clearUIWidgets();
        playerUIWidgets.add(createLobbyButton);
        playerUIWidgets.add(findLobbyButton);
        playerUIWidgets.add(mrServerBubbleLabel);

        for (Actor actor: playerUIWidgets){
            stage.addActor(actor);
        }
    }

    @Override
    public void render(float dt) {
        Gdx.gl.glClearColor(1, 1, 1, 1);

        handleInput(dt);
        update(dt);

        stage.draw();
        stage.getBatch().begin();

        for(AnimatedSprite animatedSprite : lobbyAvatars){
            animatedSprite.draw(stage.getBatch());
        }

        mrServer.draw(stage.getBatch());
        stage.getBatch().end();

        if(getStartFadeIn() && !getStartFadeOut()) fadeIn(dt);
        else if(getStartFadeOut() && !getStartFadeIn()) fadeOut(dt);
    }

    @Override
    public void handleInput(float dt) {
        if(Gdx.input.justTouched()){
            lastTouch[0] = Gdx.input.getX();
            lastTouch[1] = stage.getViewport().getScreenHeight()-Gdx.input.getY();
        }
        if(Gdx.input.isTouched()){
            float touchSensitivity = 0.1f;
            float camXPos = camera.position.x + (lastTouch[0]-Gdx.input.getX())* touchSensitivity;
            float camYPos = camera.position.y + (-stage.getViewport().getScreenHeight()+Gdx.input.getY()+lastTouch[1])* touchSensitivity;

            float a = MathUtils.clamp(camXPos, camera.viewportWidth/2, backgroundImage.getWidth()-camera.viewportWidth/2);
            float b = MathUtils.clamp(camYPos, camera.viewportHeight/2, backgroundImage.getImageHeight()-camera.viewportHeight/2);

            camera.position.set(a, b, 0);
        }
    }

    @Override
    public void update(float dt) {
        stage.getCamera().update();
        assetManager.update();
        updateAvatarTimer+= dt;
        refreshLobbyTimer += dt;

        if(updateAvatarTimer >= 0.115f){ //Update avatar dance animation.
            for(AnimatedSprite animatedSprite : lobbyAvatars){
                animatedSprite.updateTexture();
            }
            mrServer.updateTexture();
            updateAvatarTimer=0;
        }

        if(refreshLobbyTimer >= 1) {
            lobbyRefresh();
            refreshLobbyTimer=0;
        }

        stage.act(dt);

        if(playerController.getLobby().getLobbyGameStarted()){
            startFadeout = true;
        }
    }

    @Override
    public void setState() {
        currentSong.stop();
        setFadeColor(Color.BLACK);
        gvm.set(new CinematicView(gvm, playerController, assetManager, stage, skin, lobbyAvatars));
    }
}
