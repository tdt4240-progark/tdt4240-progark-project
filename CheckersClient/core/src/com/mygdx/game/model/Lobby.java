package com.mygdx.game.model;

import java.util.ArrayList;
import java.util.LinkedHashSet;

public class Lobby {

    private int ID;
    private String name;
    private int MAX_PLAYERS;
    private ArrayList<Player> players;
    private boolean lobbyGameStarted = false;

    public Lobby() {}

    public Lobby(int ID){
        this.ID = ID;
        players = new ArrayList<>();
    }

    public Lobby(int ID, String name, int MAX_PLAYERS){
        this.ID = ID;
        this.name = name;
        this.MAX_PLAYERS = MAX_PLAYERS;
        players = new ArrayList<>();
    }

    public int getID(){ return ID; }

    public boolean getLobbyGameStarted(){
        return lobbyGameStarted;
    }
    public void setLobbyGameStarted(boolean lobbyGameStarted){
        this.lobbyGameStarted = lobbyGameStarted;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public Player getPlayerByID(int playerID){

        Player returnPlayer = null;

        for (Player player : players){
            if (player.getID() == playerID) returnPlayer = player;
        }

        return returnPlayer;
    }

    public int getMaxPlayers(){
        return MAX_PLAYERS;
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }

    public LinkedHashSet<Integer> getPlayersID(){
        LinkedHashSet<Integer> set = new LinkedHashSet<Integer>();
        for(Player player : players){
            set.add((Integer) player.getID());
        }
        return set;
    }

    public void removePlayer(int playerID) {
        Player leavingPlayer = null;
        for (Player player : players) {
            if (player.getID() == playerID) {
                leavingPlayer = player;
            }
        }
        if (leavingPlayer != null) {
            players.remove(leavingPlayer);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lobby lobby = (Lobby) o;
        return ID == lobby.ID;
    }

    @Override
    public String toString() {
        return "Lobby{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", MAX_PLAYERS=" + MAX_PLAYERS +
                ", players=" + players +
                '}';
    }
}
