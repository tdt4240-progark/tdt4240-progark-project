package com.mygdx.game.model

import com.badlogic.gdx.math.Vector3

// Holds a ruleset and a board, which combined is a gamemode

class GameMode(rules:AbstractRules, board:AbstractBoard) {

    private var rules: AbstractRules
    private var board: AbstractBoard

    init {
        this.rules = rules
        this.board = board
    }

    fun getBoard(): AbstractBoard {
        return this.board
    }

    fun getRules(): AbstractRules {
        return this.rules
    }

    private fun getPosition(currentPosition: Vector3, direction: Vector3, numberOfSteps: Int = 1): Vector3 {
        val v = currentPosition.cpy()
        for (i in 1..numberOfSteps) {
            v.add(direction)
        }
        return v.cpy()
    }

    private fun isFieldClear(field: Vector3): Boolean {
        if (!this.board.fieldExists(field) || this.board.fields[field]?.hasPiece() == true) {
            return false
        }
        return true
    }

    private fun isPathJumpable(position: Vector3, direction: Vector3, destination: Vector3): Boolean {

        val v = position.cpy().add(direction)

        while (v != destination) {
            if (isFieldClear(v) || !this.board.fieldExists(v)) {
                return false
            }
            v.add(direction)
        }
        if (isFieldClear(v)) {
            return true
        }
        return false
    }

    // Populates a list with all possible basic moves
    private fun getBasicMoves(position: Vector3, moveRange: Int, frontier: ArrayList<Vector3>) {

        for (direction: Vector3 in directionalUnitVectors) {

            var destination = position.cpy()

            for (step in 1..moveRange) {
                destination.add(direction)

                if (!isFieldClear(destination)) {
                    break
                }
                frontier.add(destination.cpy())
            }
        }
    }

    // Populates a list with all possible jumps
    private fun getDefaultJumps(position: Vector3, jumpRange: Int, frontier: ArrayList<Vector3>, maxIterations: Int,  iterationNumber: Int) {

        if (iterationNumber < maxIterations) {
            for (direction: Vector3 in directionalUnitVectors) {

                var destination = getPosition(position, direction, jumpRange + 1)

                if (isPathJumpable(position, direction, destination)){
                    frontier.add(destination.cpy())
                    getDefaultJumps(destination, jumpRange, frontier, maxIterations, iterationNumber+1)
                }
            }
        }
    }

    // Choice of gamemode should be implemented here in the future
    fun getPossibleMoves(position: Vector3): ArrayList<Vector3> {

        val possibleMoves: ArrayList<Vector3> = arrayListOf()
        getBasicMoves(position, this.rules.moveRange, possibleMoves)
        getDefaultJumps(position, this.rules.jumpRange,possibleMoves, this.rules.maxJumps,0)
        return possibleMoves
    }
}
