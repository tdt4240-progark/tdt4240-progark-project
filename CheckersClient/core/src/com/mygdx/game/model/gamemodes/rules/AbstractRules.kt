package com.mygdx.game.model

import com.badlogic.gdx.math.Vector3

abstract class AbstractRules {

    abstract var startFields: List<List<Vector3>> // List containing list of startfields for each player

    abstract var moveRange: Int // How far a piece can be moved
    abstract var jumpRange: Int // How many pieces a piece can jump over
    abstract var maxJumps: Int // How many jumps can a piece do after each other

    abstract fun getPlayerStartfields(boardSlot: Int) : List<Vector3>                // Returns a list of startfields for a given player
    abstract fun getPlayerTargetFields(boardSlot: Int) : List<Vector3>               // Returns a list of targetfields for a given player
    abstract fun generateStartFields(playerCount: Int = 6): List<List<Vector3>>      // Returns a list with lists of startfields
    abstract fun getUsedBoardSlots(playerCount: Int): List<Int>                      // Returns a list with used boardslots
}