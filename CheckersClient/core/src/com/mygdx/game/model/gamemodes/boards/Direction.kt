    package com.mygdx.game.model

import com.badlogic.gdx.math.Vector3

enum class Direction {
    UPPERRIGHT,
    RIGHT,
    LOWERRIGHT,
    LOWERLEFT,
    LEFT,
    UPPERLEFT
}

// Sequence matches Direction enum above (UpperRight, Right, LowerRight, LowerLeft, Left, UpperLeft)
var directionalUnitVectors = listOf(Vector3(1F, 0F, -1F), Vector3(1F, -1F, 0F), Vector3(0F, -1F, 1F), Vector3(-1F, 0F, 1F), Vector3(-1F, 1F, 0F), Vector3(0F, 1F, -1F))