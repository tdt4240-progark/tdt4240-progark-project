package com.mygdx.game.model

import com.badlogic.gdx.math.Vector3

abstract class AbstractBoard {

    var fields = HashMap<Vector3, Field>() // Hashmap containing all fields

    abstract fun computeFields()                                                      // Computes all fields on the board
    abstract fun fieldExists(coordinates: Vector3): Boolean                           // Returns whether a field exists on board
    abstract fun placePiecesAtStart(rules: AbstractRules, playerIds: HashSet<Int>)    // Places pieces at startfields
    abstract fun movePiece(fromCoordinates: Vector3, toCoordinates: Vector3): Boolean // Moves piece
    abstract fun getBoardImagePath(): String                                          // Returns path to board image
    abstract fun getPlayerNameCoordinates(hex_side_length: Float): List<List<Float>>  // Returns a list with lists of coordinates for playernames
    fun playerLeftMidGame(playerId: Int): List<Vector3> {                             // Removes a players pieces and returns their coordinates
        var removedPieces: List<Vector3> = listOf()

        // Get coordinates of pleyers pieces
        for (coordinate: Vector3 in fields.keys) {
            if (fields[coordinate]?.getPiece()?.getOwnerId() == playerId) {
                removedPieces = removedPieces + coordinate
            }
        }
        // Remove piece from fields
        for (coordinate: Vector3 in removedPieces) {
            fields[coordinate]?.removePiece()
        }

        return removedPieces
    }
}