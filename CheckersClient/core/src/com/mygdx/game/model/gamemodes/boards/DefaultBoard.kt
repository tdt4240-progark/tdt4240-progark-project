//General board class
package com.mygdx.game.model

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Vector3
import com.mygdx.game.Constants
import java.lang.Integer.max
import java.lang.Integer.min

class DefaultBoard: AbstractBoard() {
    var r = 8

    init {
        computeFields()
    }

    override fun computeFields() {
        // https://www.redblobgames.com/grids/hexagons/#range
        for (x in -r..r) {
            loop@ for (y in max(-r, -x - r)..min(r, -x + r)) {
                val z = -x - y

                val coordinates = Vector3(x.toFloat(), y.toFloat(), z.toFloat())
                if (!fieldShouldBeAddedToBoard(coordinates)) {
                    continue@loop
                }
                fields[coordinates] = Field()
            }
        }
    }

    override fun placePiecesAtStart(rules: AbstractRules, playerIds: HashSet<Int>) {

        val startFields: List<List<Vector3>> = rules.generateStartFields(playerIds.size)

        for ((playerIndex, coordinateSet: List<Vector3>) in startFields.withIndex()) {
            for (coordinate: Vector3 in coordinateSet) {
                val piece = Piece(playerIds.elementAt(playerIndex))
                fields[coordinate]?.setPiece(piece)
            }
        }
    }

    override fun fieldExists(coordinates: Vector3): Boolean {
        return fields[coordinates] != null
    }

    override fun movePiece(fromCoordinates: Vector3, toCoordinates: Vector3): Boolean {
        val piece = fields[fromCoordinates]?.getPiece()

        if (piece != null) {
            fields[toCoordinates]?.setPiece(piece)
            fields[fromCoordinates]?.removePiece()
            return true
        }
        return false
    }

    private fun fieldShouldBeAddedToBoard(coordinates: Vector3) : Boolean {
        val x = coordinates.x
        val y = coordinates.y
        val z = coordinates.z

        // Upper right
        if (x > 4) {
            if (y < -4 || z < -4)
                return false
        }
        // Bottom
        if (z > 4) {
            if (y < -4 || x < -4)
                return false
        }
        // Upper left
        if (y > 4) {
            if (x < -4 || z < -4) {
                return false
            }
        }
        return true
    }

    override fun getBoardImagePath(): String {
        return "Game/1x/GameBoard@1x.png"
    }

    override fun getPlayerNameCoordinates(hex_side_length: Float): List<List<Float>> {
        return listOf(
                listOf(Gdx.graphics.width / 2f - Constants.PLAYER_NAME_TEXT_FIELD_WIDTH - 2.5f * hex_side_length, Gdx.graphics.height - Constants.PLAYER_NAME_TEXT_FIELD_HEIGHT - 1.5f * hex_side_length),
                listOf(Gdx.graphics.width / 2f + Constants.PLAYER_NAME_TEXT_FIELD_WIDTH + 4f * hex_side_length, Gdx.graphics.height / 2f + 4 * hex_side_length),
                listOf(Gdx.graphics.width / 2f + Constants.PLAYER_NAME_TEXT_FIELD_WIDTH + 4f * hex_side_length, Gdx.graphics.height / 2f - 5 * hex_side_length),
                listOf(Gdx.graphics.width / 2f - Constants.PLAYER_NAME_TEXT_FIELD_WIDTH - 2.5f * hex_side_length, 1.5f * hex_side_length),
                listOf(Gdx.graphics.width / 2f - Constants.PLAYER_NAME_TEXT_FIELD_WIDTH - 12f * hex_side_length, Gdx.graphics.height / 2f - 5 * hex_side_length),
                listOf(Gdx.graphics.width / 2f - Constants.PLAYER_NAME_TEXT_FIELD_WIDTH - 12f * hex_side_length, Gdx.graphics.height / 2f + 4 * hex_side_length)
        )
    }
}