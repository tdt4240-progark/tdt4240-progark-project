package com.mygdx.game.model.gamemodes.rules

import com.badlogic.gdx.math.Vector3
import com.mygdx.game.model.AbstractRules

class DefaultRules: AbstractRules() {
    override var startFields = generateStartFields()
    override var moveRange = 1
    override var jumpRange = 1
    override var maxJumps = 6

    override fun getPlayerStartfields(boardSlot: Int): List<Vector3> {
        return startFields[boardSlot]
    }

    override fun getPlayerTargetFields(boardSlot: Int): List<Vector3> {
        var slot = boardSlot

        for (i in 0..2) {
            slot++
            // Wrap around to first player
            if(slot == 6) {
                slot = 0
            }
        }
        return startFields[slot]
    }

    // Sequence: top, upperright, lowerright, bottom, lowerleft, upperleft
    override fun generateStartFields(playerCount: Int): List<List<Vector3>> {
        val startFields: MutableList<List<Vector3>> = mutableListOf()
        val allStartFields: List<List<Vector3>> = listOf(
                listOf(Vector3(1F, 4F, -5F), Vector3(2F, 3F, -5F), Vector3(3F, 2F, -5F), Vector3(4F, 1F, -5F),
                        Vector3(2F, 4F, -6F), Vector3(3F, 3F, -6F), Vector3(4F, 2F, -6F), Vector3(3F, 4F, -7F),
                        Vector3(4F, 3F, -7F), Vector3(4F, 4F, -8F)
                ),
                listOf(Vector3(5F, -1F, -4F), Vector3(5F, -2F, -3F), Vector3(5F, -3F, -2F), Vector3(5F, -4F, -1F),
                        Vector3(6F, -2F, -4F), Vector3(6F, -3F, -3F), Vector3(6F, -4F, -2F), Vector3(7F, -3F, -4F),
                        Vector3(7F, -4F, -3F), Vector3(8F, -4F, -4F)
                ),
                listOf(Vector3(4F, -5F, 1F), Vector3(3F, -5F, 2F), Vector3(2F, -5F, 3F), Vector3(1F, -5F, 4F),
                        Vector3(4F, -6F, 2F), Vector3(3F, -6F, 3F), Vector3(2F, -6F, 4F), Vector3(4F, -7F, 3F),
                        Vector3(3F, -7F, 4F), Vector3(4F, -8F, 4F)
                ),
                listOf(Vector3(-4F, -1F, 5F), Vector3(-3F, -2F, 5F), Vector3(-2F, -3F, 5F), Vector3(-1F, -4F, 5F),
                        Vector3(-4F, -2F, 6F), Vector3(-3F, -3F, 6F), Vector3(-2F, -4F, 6F), Vector3(-4F, -3F, 7F),
                        Vector3(-3F, -4F, 7F), Vector3(-4F, -4F, 8F)
                ),
                listOf(Vector3(-5F, 4F, 1F), Vector3(-5F, 3F, 2F), Vector3(-5F, 2F, 3F), Vector3(-5F, 1F, 4F),
                        Vector3(-6F, 4F, 2F), Vector3(-6F, 3F, 3F), Vector3(-6F, 2F, 4F), Vector3(-7F, 4F, 3F),
                        Vector3(-7F, 3F, 4F), Vector3(-8F, 4F, 4F)
                ),
                listOf(Vector3(-1F, 5F, -4F), Vector3(-2F, 5F, -3F), Vector3(-3F, 5F, -2F), Vector3(-4F, 5F, -1F),
                        Vector3(-2F, 6F, -4F), Vector3(-3F, 6F, -3F), Vector3(-4F, 6F, -2F), Vector3(-3F, 7F, -4F),
                        Vector3(-4F, 7F, -3F), Vector3(-4F, 8F, -4F)
                )
        )

        when (playerCount) {
            2 -> {
                startFields.add(allStartFields[0])
                startFields.add(allStartFields[3])
            }
            4 -> {
                startFields.add(allStartFields[1])
                startFields.add(allStartFields[2])
                startFields.add(allStartFields[4])
                startFields.add(allStartFields[5])
            }
            6 -> startFields.addAll(allStartFields)
            else -> {
                print("ERROR: DefaultBoard needs 2, 4 or 6 players.")
            }
        }

        return startFields
    }

    override fun getUsedBoardSlots(playerCount: Int): List<Int> {
        return when (playerCount) {
            2 -> {
                listOf(0, 3)
            }
            4 -> {
                listOf(1, 2, 4, 5)
            }
            else -> {
                listOf(0, 1, 2, 3, 4, 5)
            }
        }
    }
}

