package com.mygdx.game.model

class Piece(ownerId: Int) {

    private var ownerId: Int = ownerId

    fun getOwnerId(): Int {
        return this.ownerId
    }
}
