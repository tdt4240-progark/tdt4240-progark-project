package com.mygdx.game.model

import com.badlogic.gdx.math.Vector3
import com.mygdx.game.controllers.GameController

class Game(gameState: GameState, playerIds: LinkedHashSet<Int>) {

    private var gameState: GameState
    private var playerIds: LinkedHashSet<Int>
    private var playerTurnId: Int
    private var playerTurnIndex: Int = 0
    private var playerTurnSlot: Int
    private var playerFinishedIds: List<Int> = listOf()
    private var playerLeftIds: List<Int> = listOf()
    private var usedBoardSlots: List<Int>

    init {
        this.gameState = gameState
        this.playerIds = playerIds
        this.gameState.getBoardState().placePiecesAtStart(this.gameState.getRules(), this.playerIds)

        usedBoardSlots =  this.gameState.getRules().getUsedBoardSlots(playerIds.size)
        playerTurnSlot = usedBoardSlots[0]
        playerTurnId = playerIds.elementAt(playerTurnIndex)
    }

    fun getStartFieldCoordinates(): List<List<Vector3>> {
        return this.gameState.getRules().generateStartFields(playerIds.size)
    }

    fun fieldExists(cubeCoordinates: Vector3): Boolean {
        return this.gameState.getBoardState().fieldExists(cubeCoordinates)
    }

    fun fieldHasPiece(cubeCoordinates: Vector3): Boolean {
        return this.gameState.getBoardState().fields[cubeCoordinates]?.hasPiece() == true
    }

    private fun isLegalMove(fromCoordinates: Vector3, toCoordinates: Vector3): Boolean {
        return getGameState().getGameMode().getPossibleMoves(fromCoordinates).contains(toCoordinates)
    }

    fun movePiece(fromCoordinates: Vector3, toCoordinates: Vector3, playerId: Int, gameController: GameController): Boolean {
        // Check if players turn and if move is legal
        if (playerId == playerTurnId && isLegalMove(fromCoordinates,toCoordinates)) {

            val pieceMoved: Boolean = this.gameState.getBoardState().movePiece(fromCoordinates, toCoordinates)

            if (pieceMoved) {
                if (isPlayerFinished(playerId)) {
                    playerFinishedIds = playerFinishedIds + playerId
                    gameController.setPlayerFinished(playerId, playerFinishedIds.size)

                    // First player finished, he/she wins
                    if (playerFinishedIds.size == 1) {
                        this.gameState.setWinner(playerId)
                    }
                    // All players are finished
                    if (playerFinishedIds.size == playerIds.size - playerLeftIds.size) {
                        gameController.setGameFinished()
                        this.gameState.setFinished()
                    }
                }
                nextPlayer()
            }

            return pieceMoved
        } else {
            return false
        }
    }

    fun getPossibleMoves(fromCoordinates: Vector3): ArrayList<Vector3> {
        return getGameState().getGameMode().getPossibleMoves(fromCoordinates)
    }

    fun getPlayerTurnId(): Int {
        return playerTurnId
    }

    fun getPlayerTurnSlot(): Int {
        return usedBoardSlots[playerTurnIndex]
    }

    private fun nextPlayer() {
        do {
            playerTurnIndex++
            if (playerTurnIndex == playerIds.size) {
                playerTurnIndex = 0
            }
            playerTurnId = playerIds.elementAt(playerTurnIndex)
        } while (playerFinishedIds.contains(playerTurnId) && thereArePlayersLeft() || playerLeftIds.contains(playerTurnId))
    }

    fun getPieceOwnerId(coordinates: Vector3): Int {

        return gameState.getBoardState().fields[coordinates]?.getPiece()?.getOwnerId() ?: -1
    }

    fun isPlayerFinished(playerId: Int): Boolean {
        if (playerFinishedIds.contains(playerId)) {
            return true
        }

        val boardSlot = usedBoardSlots[playerIds.indexOf(playerId)];
        val targetFields = this.gameState.getRules().getPlayerTargetFields(boardSlot)

        for (targetField: Vector3 in targetFields) {
            if (this.gameState.getBoardState().fields[targetField]?.getPiece()?.getOwnerId() != playerId) {
                return false
            }
        }

        return true
    }

    private fun getGameState(): GameState {
        return this.gameState
    }

    fun getUsedBoardSlots(): List<Int> {
        return usedBoardSlots
    }

    fun getPlayerNameCoordinates(hex_side_length: Float): List<List<Float>> {
        return gameState.getBoardState().getPlayerNameCoordinates(hex_side_length)
    }

    fun setPlayerLeftMidGame(playerId: Int): List<Vector3> {
        playerLeftIds = playerLeftIds + playerId
        if (playerTurnId == playerId) {
            nextPlayer()
        }
        return this.gameState.getBoardState().playerLeftMidGame(playerId)
    }

    private fun thereArePlayersLeft(): Boolean {
        return playerFinishedIds.size < playerIds.size - playerLeftIds.size
    }

    fun getPlayerIds(): LinkedHashSet<Int> {
        return playerIds
    }

    fun getBoardImagePath(): String {
        return this.gameState.getBoardState().getBoardImagePath()
    }
}
