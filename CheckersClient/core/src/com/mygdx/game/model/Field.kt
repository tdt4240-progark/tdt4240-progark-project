//Inside gameState, data for a specific field
package com.mygdx.game.model

import java.lang.Exception

class Field {

    private var piece: Piece?
    //private var powerup: Powerup?

    init {
        this.piece = null
    }

    fun setPiece(piece: Piece) {
        if (this.piece != null) {
            throw Exception("Field already holds a piece.")
        } else {
            this.piece = piece
        }
    }

    fun getPiece(): Piece? {
        return this.piece
    }

    fun hasPiece(): Boolean {
        if (this.piece == null) {
            return false
        }
        return true
    }

    fun removePiece() {
        this.piece = null
    }
}