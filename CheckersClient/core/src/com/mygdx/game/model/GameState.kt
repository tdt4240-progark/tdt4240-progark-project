package com.mygdx.game.model

/*
Holds the state of the game and all data related to this.
*/
class GameState(gameMode: GameMode) {

    private var gameMode: GameMode
    private var boardState: AbstractBoard
    private var rules: AbstractRules
    private var isStarted: Boolean
    private var winner: Int?
    private var isFinished: Boolean = false

    init {
        this.gameMode = gameMode
        this.rules = gameMode.getRules()
        this.boardState = gameMode.getBoard()
        this.isStarted = false
        this.winner = null
    }

    fun getBoardState(): AbstractBoard {
        return boardState
    }

    fun getRules(): AbstractRules {
        return rules
    }

    fun getGameMode(): GameMode {
        return this.gameMode
    }

    fun setWinner(winner: Int) {
        this.winner = winner
    }

    fun setFinished() {
        isFinished = true
    }
}