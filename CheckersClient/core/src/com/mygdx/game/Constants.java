package com.mygdx.game;

import java.util.HashMap;

public class Constants {
    public static final int PLAYER_NAME_TEXT_FIELD_WIDTH = 340;
    public static final int PLAYER_NAME_TEXT_FIELD_HEIGHT = 80;
    public static final HashMap<Integer, String> AVAILABLEAVATARSHASHMAP = new HashMap<Integer, String>() {{
        put(0, "RegularGuy");
        put(1, "HipsterGirl");
        put(2, "HighSchoolGuy");
        put(3, "RocketGuy");
        put(4, "AsianGirl");
        put(5, "Grandma");
    }};
}
