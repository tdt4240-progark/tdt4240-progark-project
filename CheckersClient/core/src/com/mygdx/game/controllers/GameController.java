package com.mygdx.game.controllers;

import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.controllers.commands.cPlayerPieceMove;
import com.mygdx.game.model.Game;
import com.mygdx.game.model.Player;
import com.mygdx.game.views.PlayView;
import com.mygdx.game.views.tokens.StarPiece;

import java.util.ArrayList;
import java.util.List;

public class GameController {

    private final Game model;
    private final PlayView view;

    private StarPiece activePiece;                                  // Previously clicked piece
    private Vector3 previousCoordinateClicked;                      // Previously clicked coordinate
    private final List<List<Float>> playerNameTextFieldCoordinates; // Coordinates of playernames
    private final PlayerController playerController;

    public GameController(Game model, PlayView view, PlayerController playerController) {
        this.model = model;
        this.view = view;
        this.playerController = playerController;

        this.activePiece = null;
        this.previousCoordinateClicked = null;

        // Get used boardslots and coordinates to place playernames
        List<Integer> usedBoardSlots = this.model.getUsedBoardSlots();
        playerNameTextFieldCoordinates = this.model.getPlayerNameCoordinates(this.view.hex_side_length);

        // Initialize pieces, place playernames, turn-indicator and leave-button
        this.view.initializePieces(this.model.getStartFieldCoordinates());
        this.view.placePlayerNames(usedBoardSlots, playerNameTextFieldCoordinates);
        this.view.placeTurnIndicator(playerNameTextFieldCoordinates.get(this.model.getPlayerTurnSlot()));
    }

    public void handleClick(float x, float y) {
        Vector3 cubeCoordinates = UtilsKt.pixelToCube(x, y, this.view.hex_side_length);

        // If field exists at clicked coordinates
        if (this.model.fieldExists(cubeCoordinates)) {
            // If field has piece
            if (this.model.fieldHasPiece(cubeCoordinates)) {
                // If another piece is already active, deactivate it
                if (activePiece != null) {
                    activePiece.setRotateHead(false);
                    this.view.removePossibleMoves();
                }
                // If clicked piece is owned by player and player is not finished, activate piece
                if(this.model.getPieceOwnerId(cubeCoordinates) == playerController.getPlayer().getID() && !this.model.isPlayerFinished(playerController.getPlayer().getID())) {
                    activePiece = this.view.getPiece(cubeCoordinates);
                    activePiece.setRotateHead(true);
                    previousCoordinateClicked = cubeCoordinates;

                    this.view.placePossibleMoves(model.getPossibleMoves(previousCoordinateClicked));
                }

            } else { // Field does NOT have piece
                if (activePiece != null) {
                    // Try to move piece in model. If successful, move piece in view
                    if (this.model.movePiece(previousCoordinateClicked, cubeCoordinates, playerController.getPlayer().getID(), this)) {
                        this.view.movePiece(activePiece, cubeCoordinates);
                        activePiece.setRotateHead(false);

                        playerController.getNetWorkController().sendToServer(new cPlayerPieceMove(previousCoordinateClicked, cubeCoordinates, playerController.getLobby().getID(), playerController.getPlayer().getID()));

                        // Reset previously clicked piece and coordinate
                        activePiece = null;
                        previousCoordinateClicked = null;

                        // Remove possible move indicators and place turn indicator at next player
                        this.view.removePossibleMoves();
                        this.view.placeTurnIndicator(playerNameTextFieldCoordinates.get(this.model.getPlayerTurnSlot()));
                    } else {
                        System.out.println("Move was not allowed"); // TODO: Give feedback (move was not valid/not executed)
                    }
                }
            }
        }
    }

    // External player moved piece (called by cPlayerPieceMove command)
    public void playerMovedPiece(Vector3 fromCoordinates, Vector3 toCoordinates, int playerId) {
        // Try to move piece in model. If successful, move piece in view and place turn indicator at next player
        if (this.model.movePiece(fromCoordinates, toCoordinates, playerId, this)) {
            this.view.moveOtherPlayerPiece(fromCoordinates, toCoordinates);
            this.view.placeTurnIndicator(playerNameTextFieldCoordinates.get(this.model.getPlayerTurnSlot()));
        } else {
            System.out.println("ERROR: Could not execute moving of other players piece"); // TODO: Give feedback
        }
    }

    // External player left midgame (called by CLobbyLeave command)
    public void setPlayerLeftMidGame(int playerId) {
        // Tell the model that the player left. Tell view to remove those pieces
        List<Vector3> removedPieceCoordinates = model.setPlayerLeftMidGame(playerId);
        this.view.removePieces(removedPieceCoordinates);
        setPlayerFinished(playerId, -1);
        this.view.placeTurnIndicator(playerNameTextFieldCoordinates.get(model.getPlayerTurnSlot()));
    }

    public void setPlayerFinished(int playerId, int place) {
        List<Integer> playerIds = new ArrayList(model.getPlayerIds());
        int playerIndex = playerIds.indexOf(playerId);
        this.view.setPlayerFinished(playerIndex, place);
    }

    public void setGameFinished() {
        this.view.setGameFinished();
    }
}
