package com.mygdx.game.controllers;

import com.badlogic.gdx.math.Vector3;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.mygdx.game.controllers.commands.Command;
import com.mygdx.game.controllers.commands.*;
import com.mygdx.game.model.Lobby;
import com.mygdx.game.model.Player;

import java.io.IOException;
import java.util.ArrayList;

public class NetworkController {

    private final Client client;
    private final Kryo kryo;

    private final boolean devMode = false;

    public NetworkController(){
        this.client = new Client();
        client.start();
        try {
            String IP4_LAN_ADDRESS = "192.168.87.23";
            client.connect(10000, IP4_LAN_ADDRESS, 54555);

        } catch (IOException e) {
            e.printStackTrace();
        }
        kryo = client.getKryo();
        registerClasses();
    }

    public void sendToServer(Command command){
        client.sendTCP(command);
    }

    public void registerClasses(){
        kryo.register(ArrayList.class, 100);
        kryo.register(Lobby.class, 5);
        kryo.register(Player.class, 6);
        kryo.register(cPlayerJoin.class, 7);
        kryo.register(cPlayerPieceMove.class, 8);
        kryo.register(cPlayerPieceClick.class, 9);
        kryo.register(cLobbyCreate.class, 20);
        kryo.register(cLobbyJoin.class, 21);
        kryo.register(cLobbyLeave.class, 22);
        kryo.register(cLobbyDelete.class, 23);
        kryo.register(cLobbyGetList.class, 24);
        kryo.register(cSetIndexAvatar.class, 25);
        kryo.register(cSetPlayerReady.class, 26);
        kryo.register(cSetPlayerName.class, 27);
        kryo.register(cStartGame.class, 28);
        kryo.register(Vector3.class, 29);
    }

    public Client getClient() { return client; }

    private NetworkController getNetworkController() { return this; }
}
