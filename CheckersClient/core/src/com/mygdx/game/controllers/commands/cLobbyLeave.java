package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.GameController;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.Lobby;

import java.util.ArrayList;
import java.util.List;

public class cLobbyLeave extends Command{

    public cLobbyLeave() { super("cLobbyLeave"); }

    int lobbyId;
    int playerId;

    public cLobbyLeave(int id, int playerId) {
        super("cLobbyLeave");
        this.lobbyId = id;
        this.playerId = playerId;

        ArrayList<Object> data = new ArrayList<Object>();
        data.add(lobbyId);
        data.add(playerId);

        this.data = data;
    }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if (data instanceof ArrayList) {
            List<Object> receivedData = (ArrayList<Object>) data;

            lobbyId = (int) receivedData.get(0);
            playerId = (int) receivedData.get(1);

            if (playerId == playerController.getPlayer().getID()) {
                System.out.println("Request to leave lobby successful");
                playerController.setLobby(new Lobby(-1));
                playerController.getPlayer().setIsPlayerReady(false);
            } else {
                System.out.println("Player with id " + playerId + " left");

                GameController gameController = playerController.getGameController();
                if (gameController != null) {
                    gameController.setPlayerLeftMidGame(playerId);
                }

                playerController.getLobby().removePlayer(playerId);
            }
        }
    }
}
