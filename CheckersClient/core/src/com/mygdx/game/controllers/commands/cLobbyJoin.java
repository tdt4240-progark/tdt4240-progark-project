package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.Lobby;
import com.mygdx.game.model.Player;

public class cLobbyJoin extends Command{

    public cLobbyJoin() { super("cLobbyJoin"); }

    public cLobbyJoin(int id) {
        super("cLobbyJoin", (Integer) id);
    }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if(data instanceof Lobby){
            Lobby lobby = (Lobby) data;
            if (lobby.getID() == -1) System.out.println("Request to join lobby failed");
            else {
                playerController.setLobby(lobby);
                System.out.printf("Request to join lobby accepted. Lobby ID = %d \n", lobby.getID());
            }
        }
    }
}
