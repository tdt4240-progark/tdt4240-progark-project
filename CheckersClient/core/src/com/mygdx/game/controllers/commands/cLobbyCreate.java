package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.Lobby;

import java.util.ArrayList;

public class cLobbyCreate extends Command {

    public cLobbyCreate() { super("cLobbyCreate"); }

    public cLobbyCreate(String name, int MAX_PLAYERS) {
        super("cLobbyCreate");
        ArrayList<Object> data = new ArrayList<Object>();
        data.add(name);
        data.add(MAX_PLAYERS);
        this.data = data;
    }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if(data instanceof Lobby){
            Lobby lobby = (Lobby) data;
            if (lobby.getID() == -1) System.out.println("Request to create lobby failed");
            else {
                playerController.setLobby(lobby);
                System.out.printf("Request to create lobby successful. Lobby ID = %d \n", lobby.getID());
            }
        }
    }
}
