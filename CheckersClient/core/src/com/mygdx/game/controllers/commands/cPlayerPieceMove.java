package com.mygdx.game.controllers.commands;

import com.badlogic.gdx.math.Vector3;
import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.PlayerController;

import java.util.ArrayList;
import java.util.List;

public class cPlayerPieceMove extends Command {

    Vector3 fromCoordinates;
    Vector3 toCordinates;
    int lobby_id;
    int playerId;

    public cPlayerPieceMove() {
        super("cPlayerPieceMove");
    }

    public cPlayerPieceMove(Vector3 fromCoordinates, Vector3 toCoordinates, int lobbyId, int playerId) {
        super("cPlayerPieceMove");

        this.fromCoordinates = fromCoordinates;
        this.toCordinates = toCoordinates;
        this.lobby_id = lobbyId;
        this.playerId = playerId;

        ArrayList<Object> data = new ArrayList<Object>();
        data.add(fromCoordinates);
        data.add(toCoordinates);
        data.add(playerId);

        this.data = data;
    }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if (data instanceof ArrayList) {
            List<Object> receivedData = (ArrayList<Object>) data;

            Vector3 fromCoordinates = (Vector3) receivedData.get(0);
            Vector3 toCoordinates = (Vector3) receivedData.get(1);
            int playerId = (int) receivedData.get(2);

            playerController.getGameController().playerMovedPiece(fromCoordinates, toCoordinates, playerId);
            System.out.printf("Received move from other player. Player ID = %d \n", playerId);
        }
    }
}
