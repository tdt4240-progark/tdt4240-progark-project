package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.PlayerController;

public class cSetPlayerReady extends Command{

    public cSetPlayerReady() { super("cSetPlayerReady"); }

    private int lobbyID;
    private int playerID;

    public cSetPlayerReady(boolean isPlayerReady, int lobbyID) {
        super("cSetPlayerReady", isPlayerReady);
        this.lobbyID = lobbyID;
        this.playerID = playerID;
    }

    public cSetPlayerReady(boolean isPlayerReady, int lobbyID, int playerID) {
        super("cSetPlayerReady", isPlayerReady);
        this.lobbyID = lobbyID;
        this.playerID = playerID;
    }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if(data instanceof Boolean){
            boolean isPlayerReady = (Boolean) data;
            if(playerID == playerController.getPlayer().getID()) playerController.getPlayer().setIsPlayerReady(isPlayerReady);
            playerController.getLobby().getPlayerByID(playerID).setIsPlayerReady(isPlayerReady);

            System.out.printf("Player status updated. Is player %d currently ready? %b \n", playerID, isPlayerReady);
        }
    }
}
