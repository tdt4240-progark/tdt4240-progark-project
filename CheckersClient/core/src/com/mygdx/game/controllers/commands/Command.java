package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.NetworkController;
import com.mygdx.game.controllers.PlayerController;

public class Command{

    protected String text;
    protected Object data;

    public Command(String text, Object data){
        this.text = text;
        this.data = data;
    }

    public Command(String text){
        this.text = text;
        this.data = "None";
    }


    public void execute(PlayerController playerController, Connection connection){}

    public String getText(){ return text; }

    public Object getData(){ return data; }

    public void setData(Object object){ data = object; }

    @Override
    public String toString() {
        return "Command{" +
                "text='" + text + '\'' +
                "data='" + data.toString() + '\'' +
                '}';
    }
}