package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.Lobby;

public class cLobbyDelete extends Command{

    public cLobbyDelete() { super("cLobbyDelete"); }

    public cLobbyDelete(int id) { super("cLobbyDelete", (Integer) id); }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if(data instanceof Integer){
            int result = (int) data;
            if (result == -1) System.out.println("Failed to delete lobby");
            else {
                playerController.setLobby(new Lobby(-1));
                System.out.println("Lobby was deleted");
            }
        }
    }
}
