package com.mygdx.game.controllers.commands;


import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.PlayerController;

public class cStartGame extends Command{

    public cStartGame() { super("cStartGame"); }

    public cStartGame(int id) { super("cStartGame", (Integer) id); }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        playerController.getLobby().setLobbyGameStarted(true);
        System.out.println("GAME HAS STARTED");

    }
}
