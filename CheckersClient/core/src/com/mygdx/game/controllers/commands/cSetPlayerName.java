package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.Lobby;
import com.mygdx.game.model.Player;

import org.javatuples.Pair;


public class cSetPlayerName extends Command{

    public cSetPlayerName() { super("cSetPlayerName"); }

    public cSetPlayerName(String name) {
        super("cSetPlayerName", name);
    }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if(data instanceof String){
            String name = (String) data;
            playerController.getPlayer().setPlayerName(name);
            System.out.printf("Player name is updated on server. %s \n", name);
        }
    }
}
