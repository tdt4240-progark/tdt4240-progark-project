package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.NetworkController;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.Player;

public class cPlayerJoin extends Command {

    public cPlayerJoin() {
        super("cPlayerJoin");
    }

    public cPlayerJoin(int indexAvatar) {
        super("cPlayerJoin", (Integer) indexAvatar);
    }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if(data instanceof Player){
            Player player = (Player) data;
            if (player.getID() == -1) System.out.println("Request to join denied from Server");
            else {
                playerController.setPlayer(player);
                System.out.printf("Request to join accepted. Player ID = %d \n", player.getID());
            }
        }
    }
}