package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.NetworkController;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.Lobby;

public class cSetIndexAvatar extends Command {

    public cSetIndexAvatar() { super("cSetIndexAvatar"); }

    public cSetIndexAvatar(int indexAvatar) {
        super("cSetIndexAvatar", indexAvatar);
    }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if(data instanceof Integer){
            int indexAvatar = (int) data;
            if (indexAvatar == -1) System.out.println("Failed to set indexAvatar.");
            else {
                playerController.getPlayer().setIndexAvatar(indexAvatar);
            }
        }
    }
}