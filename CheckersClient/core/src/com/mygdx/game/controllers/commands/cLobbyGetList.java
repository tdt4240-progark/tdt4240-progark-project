package com.mygdx.game.controllers.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.model.Lobby;

import java.util.ArrayList;

public class cLobbyGetList extends Command{

    public cLobbyGetList(){ super("cLobbyGetList"); }

    @Override
    public void execute(PlayerController playerController, Connection connection){
        if(data instanceof ArrayList){
            ArrayList<Lobby> lobbies = (ArrayList) (data);
            playerController.setLobbies(lobbies);
            System.out.printf("Received lobby list: %s \n", data.toString());
        }
    }
}

