package com.mygdx.game.controllers;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.mygdx.game.controllers.commands.*;
import com.mygdx.game.model.Lobby;
import com.mygdx.game.model.Player;

import java.util.ArrayList;
import java.util.LinkedList;


public class PlayerController {

    private final NetworkController net;
    private final LinkedList<Command> receivedCommands;
    private Player player;               // This is the current player
    private Lobby lobby;                 // Current lobby
    private ArrayList<Lobby> lobbies;    // List of all lobbies
    private GameController gameController;

    public PlayerController(NetworkController networkController){
        net = networkController;
        player = new Player(-1);
        lobby = new Lobby(-1);
        lobbies = new ArrayList<>();
        receivedCommands = new LinkedList<>();

        net.getClient().addListener(new Listener() {
            public void received (Connection connection, Object object) {
                if (object instanceof Command) {
                    Command command = (Command) object;
                    System.out.println("Received: "+ command.toString());
                    command.execute(getPlayerController(), connection);
                    receivedCommands.removeFirst();
                    receivedCommands.addFirst(command);
                }
            }
        });
        connectPlayer(0);
    }

    private void sendCommand(Command command){
        receivedCommands.addFirst(null);
        net.sendToServer(command);
    }

    public void lobbyGetList(){ sendCommand(new cLobbyGetList()); }

    public void setLobbyPlayerReady(boolean isPlayerReady){
        sendCommand(new cSetPlayerReady(isPlayerReady, lobby.getID(), player.getID()));
    }

    public void connectPlayer(int avatarIndex){ sendCommand(new cPlayerJoin(avatarIndex));}

    public void createLobby(String name, int MAX_PLAYERS){ sendCommand(new cLobbyCreate(name, MAX_PLAYERS)); }

    public void updateIndexAvatar(int indexAvatar){ sendCommand(new cSetIndexAvatar(indexAvatar)); }

    public void playerSetName(String name) {
        sendCommand(new cSetPlayerName(name));
    }

    public void deleteLobby(int id){ sendCommand(new cLobbyDelete(id)); }

    public void joinLobby(int id){ sendCommand(new cLobbyJoin(id)); }

    public void leaveLobby(int id) { sendCommand(new cLobbyLeave(id, getPlayer().getID())); }

    public Lobby getLobby(){ return lobby; }

    public ArrayList<Lobby> getLobbyList(){ return lobbies; }

    public Player getPlayer(){ return player; }

    public void setPlayer(Player player){ this.player = player; }

    public void setLobby(Lobby lobby){ this.lobby = lobby; }

    public void setLobbies(ArrayList<Lobby> lobbies){ this.lobbies = lobbies; }

    public boolean isOwningPlayerInLobby(){
        try{
            if(lobby==null){
                return false;
            }
            else return lobby.getID() != -1;
        }catch (NullPointerException e){
            return false;
        }
    }

    public PlayerController getPlayerController(){ return this; }

    public NetworkController getNetWorkController() { return net; }

    public Command getLastCommand(){ return receivedCommands.getFirst(); }

    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }

    public GameController getGameController() {
        return gameController;
    }
}
