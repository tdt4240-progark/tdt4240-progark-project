package com.mygdx.game.controllers

import com.badlogic.gdx.math.Vector3
import java.util.*
import kotlin.math.roundToInt
import kotlin.math.sqrt

// The calculations in the two following functions were found here:
// https://stackoverflow.com/questions/2459402/hexagonal-grid-coordinates-to-pixel-coordinates

fun cubeToPixel(vector: Vector3, hexSideLength: Float = 1F): Array<Float> {
    val pixelX = sqrt(3F) * hexSideLength * ( (vector.z / 2F) + vector.x )
    val pixelY = 3F/2F * hexSideLength * vector.z

    return arrayOf(pixelX, pixelY)
}

fun pixelToCube(x: Float, y: Float, hexSideLength: Float = 1F) : Vector3 {
    val cubeX = ((sqrt(3F) / 3F * x - y / 3F) / hexSideLength).roundToInt()
    val cubeY = (-(sqrt(3F) / 3F * x + y / 3F) / hexSideLength).roundToInt()
    val cubeZ = (2F / 3F * y / hexSideLength).roundToInt()

    return Vector3(cubeX.toFloat(), cubeY.toFloat(), cubeZ.toFloat())
}

fun cubeCoordinateSetToPixel(cubeCoordinateSet: List<List<Vector3>>, hexSideLength: Float = 1F): List<List<Array<Float>>> {
    val startFieldCoordinatesPixel: MutableList<List<Array<Float>>> = ArrayList()

    for (coordinateSet in cubeCoordinateSet) {
        val coordinateSetPixel: MutableList<Array<Float>> = ArrayList()

        for (coordinate in coordinateSet) {
            coordinateSetPixel.add(cubeToPixel(coordinate, hexSideLength))
        }
        startFieldCoordinatesPixel.add(coordinateSetPixel)
    }
    return startFieldCoordinatesPixel
}