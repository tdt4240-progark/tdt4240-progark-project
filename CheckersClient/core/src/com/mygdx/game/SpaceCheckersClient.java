package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.controllers.NetworkController;
import com.mygdx.game.controllers.PlayerController;
import com.mygdx.game.views.CinematicView;
import com.mygdx.game.views.GameViewManager;
import com.mygdx.game.views.LoadingView;
import java.util.ArrayList;

public class SpaceCheckersClient extends ApplicationAdapter {

	PlayerController playerController;

	private GameViewManager gvm;

	@Override
	public void create () {
		Gdx.gl.glClearColor(0, 0, 1, 1);

		Viewport viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Stage stage = new Stage(viewport);
		Gdx.input.setInputProcessor(stage);

		// Initialize controllers..
		NetworkController networkController = new NetworkController();
		playerController = new PlayerController(networkController);

		// Initialize Views..
		AssetManager assetManager = new AssetManager(new InternalFileHandleResolver());
		Skin skin = new Skin(Gdx.files.internal("UISkins/glassy/skin/glassy-ui.json"));
		skin.getFont("font").getData().setScale(1.4F);
		gvm = new GameViewManager();
		gvm.push(new LoadingView(gvm, playerController, assetManager, stage, skin));
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		float dt = Gdx.graphics.getDeltaTime();
		gvm.render(dt);
	}
}
