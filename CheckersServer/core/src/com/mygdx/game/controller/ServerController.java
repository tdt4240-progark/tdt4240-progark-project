package com.mygdx.game.controller;

import com.badlogic.gdx.math.Vector3;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.mygdx.game.controller.commands.*;
import com.mygdx.game.model.Lobby;
import com.mygdx.game.model.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class ServerController {

    private final int MAX_PLAYERS = 8;
    private final int MAX_LOBBIES = 40;

    private final ArrayList<Player> players;
    private final ArrayList<Lobby> lobbies;
    private int lobby_id;          //Progressive number used to generate the id for a new lobby

    private final Server server;
    private final Kryo kryo;

    public ServerController(){
        server = new Server();
        server.start();
        try {
            server.bind(54555);
        } catch (IOException e) {
            e.printStackTrace();
        }
        kryo = server.getKryo();
        registerClasses();

        players = new ArrayList<Player>();
        lobbies = new ArrayList<Lobby>();
        lobby_id = -1;

        server.addListener(new Listener() {
            public void received(Connection connection, Object object) {
                if (object instanceof Command) {
                    Command command = (Command) object;
                    System.out.println("Received: " + command.toString());
                    command.execute(getNetworkController(), connection);
                }
            }
            public void disconnected (Connection connection) {
                playerKick(connection.getID());
            }
        });
    }

    public Player playerJoin(int newID, int indexAvatar){
        if(players.size() < MAX_PLAYERS){
            Player player = new Player(newID, indexAvatar);
            players.add(player);
            return player;
        }
        return new Player(-1);
    }

    public void playerKick(int id){
        int index = players.indexOf(new Player(id));
        if (index!= -1) {
            if (lobbies.size() > 0) {
                for(Lobby lobby : lobbies){
                    if(lobby.getPlayersID().contains(id)){
                        ArrayList<Connection> connections = getConnections(lobby);
                        cLobbyLeave command =  new cLobbyLeave(leaveLobby(id, lobby.getID()));
                        for(Connection c : connections){
                            c.sendTCP(command);
                        }
                        break;
                    }
                }
            }
            players.remove(index);
        }
    }

    public Lobby createLobby(String name, int MAX_PLAYERS){
        if(lobbies.size() < MAX_LOBBIES){
            lobby_id++;
            Lobby lobby = new Lobby(lobby_id, name, MAX_PLAYERS);
            lobbies.add(lobby);
            return lobby;
        }
        return new Lobby(-1);
    }

    public int deleteLobby(int id){
        int index = lobbies.indexOf(new Lobby(id));
        if (index!= -1) lobbies.remove(index);
        return index;
    }

    public Lobby joinLobby(int playerID, int lobbyID){
        Lobby lobby = getLobby(lobbyID);
        Player player = getPlayer(playerID);
        if (lobby.getID() != -1 && player.getID() != -1) {
            if(lobby.playerJoin(player) != -1){
                lobbies.set(lobbies.indexOf(lobby), lobby);
                return lobby;
            }
        }
        return new Lobby(-1);
    }

    public Lobby leaveLobby(int playerID, int lobbyID){
        Lobby lobby = getLobby(lobbyID);
        Player player = getPlayer(playerID);
        if (lobby.getID() != -1 && player.getID() != -1) {
            if(lobby.playerKick(player) != -1){
                if(lobby.getPlayersCount() == 0) deleteLobby(lobbyID);
                else lobbies.set(lobbies.indexOf(lobby), lobby);
                return lobby;
            }
        }
        return new Lobby(-1);
    }

    public Lobby getLobby(int lobbyID){
        int lobby_idx = lobbies.indexOf(new Lobby(lobbyID));
        if (lobby_idx != -1){
            return lobbies.get(lobby_idx);
        }
        return new Lobby(-1);
    }

    public Player getPlayer(int playerID){
        int player_idx =  players.indexOf(new Player(playerID));
        if (player_idx != -1){
            return players.get(player_idx);
        }
        return new Player(-1);
    }

    public ArrayList<Lobby> getLobbies(){ return lobbies; }

    public ArrayList<Player> getPlayers(){ return players; }

    public void registerClasses(){
        kryo.register(ArrayList.class, 100);
        kryo.register(Lobby.class, 5);
        kryo.register(Player.class, 6);
        kryo.register(cPlayerJoin.class, 7);
        kryo.register(cPlayerPieceMove.class, 8);
        kryo.register(cPlayerPieceClick.class, 9);
        kryo.register(cLobbyCreate.class, 20);
        kryo.register(cLobbyJoin.class, 21);
        kryo.register(cLobbyLeave.class, 22);
        kryo.register(cLobbyDelete.class, 23);
        kryo.register(cLobbyGetList.class, 24);
        kryo.register(cSetIndexAvatar.class, 25);
        kryo.register(cSetPlayerReady.class, 26);
        kryo.register(cSetPlayerName.class, 27);
        kryo.register(cStartGame.class, 28);
        kryo.register(Vector3.class, 29);
        kryo.register(List.class, 30);
    }

    public ServerController getNetworkController() { return this; }

    public ArrayList<Connection> getConnections(Lobby lobby){
        HashSet<Integer> players_id = lobby.getPlayersID();
        ArrayList<Connection> connections = new ArrayList<Connection>();
        for (Connection c : server.getConnections()) {
            if (players_id.contains(c.getID())) connections.add(c);
        }
        return connections;
    }
}
