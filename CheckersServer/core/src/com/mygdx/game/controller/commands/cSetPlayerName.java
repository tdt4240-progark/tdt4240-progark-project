package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;

public class cSetPlayerName extends Command{

    public cSetPlayerName() { super("cLobbyJoin"); }

    public cSetPlayerName(String name) {
        super("cLobbyJoin", name);
    }

    @Override
    public void execute(ServerController net, Connection connection){
        if(data instanceof String){
            String name = (String) data;
            net.getPlayer(connection.getID()).setPlayerName(name);
            connection.sendTCP(this);
            System.out.printf("Player name is updated. %s \n", name);
        }
    }
}