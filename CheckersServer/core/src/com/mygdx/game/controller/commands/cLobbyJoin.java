package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;
import com.mygdx.game.model.Lobby;

public class cLobbyJoin extends Command{

    public cLobbyJoin() { super("cLobbyJoin"); }

    //public cLobbyJoin(int id) { super("cLobbyJoin", (Integer) id); }

    @Override
    public void execute(ServerController net, Connection connection){
        if(data instanceof Integer){
            int lobby_id = (int) data;
            data = net.joinLobby(connection.getID(), lobby_id);
            System.out.printf("Request from Player w. ID: %d to join Lobby w. ID: %d. Returning Lobby w. ID: %d \n",
                    connection.getID(), lobby_id, ((Lobby)data).getID());
            for (Connection c : net.getConnections(net.getLobby(lobby_id))) {
                c.sendTCP(this);
            }
        }
    }
}
