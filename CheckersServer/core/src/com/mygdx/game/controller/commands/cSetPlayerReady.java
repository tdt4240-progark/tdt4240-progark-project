package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;
import com.mygdx.game.model.Lobby;

public class cSetPlayerReady extends Command{

    public cSetPlayerReady() { super("cSetPlayerReady"); }

    private int lobbyID;
    private int playerID;

    public cSetPlayerReady(boolean isPlayerReady, int lobbyID) {
        super("cSetPlayerReady", isPlayerReady);
        this.lobbyID = lobbyID;
        this.playerID = playerID;
    }

    public cSetPlayerReady(boolean isPlayerReady, int lobbyID, int playerID) {
        super("cSetPlayerReady", isPlayerReady);
        this.lobbyID = lobbyID;
        this.playerID = playerID;
    }

    @Override
    public void execute(ServerController net, Connection connection){
        if(data instanceof Boolean){
            boolean isPlayerReady = (Boolean) data;
            net.getPlayer(connection.getID()).setIsPlayerReady(isPlayerReady);
            Lobby lobby = net.getLobby(lobbyID);

            for (Connection c : net.getConnections(lobby)) {
                c.sendTCP(new cSetPlayerReady(isPlayerReady, lobbyID, connection.getID()));
            }

            if(lobby.isFullAndReady()){
                for (Connection c : net.getConnections(lobby)) {
                    c.sendTCP(new cStartGame());
                    lobby.setLobbyGameStarted(true);
                }
                System.out.printf("Lobby %b is about to start. \n", lobbyID);
            }
            System.out.printf("Player status updated. Is player %d currently ready? %b \n", playerID, isPlayerReady);
        }
    }
}