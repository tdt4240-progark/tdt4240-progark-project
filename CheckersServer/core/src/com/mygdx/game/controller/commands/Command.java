package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;

public class Command{

    protected String text;
    protected Object data;

    public Command(String text, Object data){
        this.text = text;
        this.data = data;
    }

    public Command(String text){
        this.text = text;
        this.data = "None";
    }

    public void execute(ServerController net, Connection connection){}

    public String getText(){ return text; }

    public Object getData(){ return data; }

    @Override
    public String toString() {
        return "Command{" +
                "text='" + text + '\'' +
                "data='" + data.toString() + '\'' +
                '}';
    }
}