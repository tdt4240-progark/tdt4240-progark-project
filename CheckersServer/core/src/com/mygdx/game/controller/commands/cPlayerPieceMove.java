package com.mygdx.game.controller.commands;

import com.badlogic.gdx.math.Vector3;
import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;

import java.util.ArrayList;

public class cPlayerPieceMove extends Command {

    Vector3 fromCoordinates;
    Vector3 toCordinates;
    int lobby_id;
    int playerId;

    public cPlayerPieceMove() {
        super("cPlayerPieceMove");
    }

    /*public cPlayerPieceMove(Vector3 fromCoordinates, Vector3 toCoordinates, int lobbyId, int playerId) {
        super("cPlayerPieceMove");

        this.fromCoordinates = fromCoordinates;
        this.toCordinates = toCoordinates;
        this.lobby_id = lobbyId;
        this.playerId = playerId;

        ArrayList<Object> data = new ArrayList<Object>();
        data.add(fromCoordinates);
        data.add(toCoordinates);
        data.add(playerId);
        this.data = data;
    }*/

    @Override
    public void execute(ServerController net, Connection connection){
        if (data instanceof ArrayList) {
            System.out.println("Checking if valid move.. If valid, moving player piece from A to B... Updating model.. Sending move to clients....");
        }

        for (Connection c : net.getConnections(net.getLobby(lobby_id))) {
            if (c != connection) {
                c.sendTCP(this);
            }
        }
    }
}


