package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;
import com.mygdx.game.model.Lobby;

public class cLobbyDelete extends Command{

    public cLobbyDelete() { super("cLobbyDelete"); }

    @Override
    public void execute(ServerController net, Connection connection){
        if(data instanceof Integer) {
            int lobby_id = (int) data;
            Lobby lobby = net.getLobby(lobby_id);

            // Notify all players in the lobby
            data = (Integer) net.deleteLobby(lobby_id);
            System.out.printf("Request from Player w. ID: %d to delete Lobby w. ID: %d. Returning code: %d \n",
                    connection.getID(), lobby_id, ((Integer) data));
            for (Connection c : net.getConnections(lobby)) {
                c.sendTCP(this);
            }
        }
    }
}
