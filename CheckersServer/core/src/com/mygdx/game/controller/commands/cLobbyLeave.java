package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;
import com.mygdx.game.model.Lobby;

import java.util.ArrayList;
import java.util.List;

public class cLobbyLeave extends Command{

    public cLobbyLeave() { super("cLobbyLeave"); }

    int lobbyId;
    int playerId;

    public cLobbyLeave(Lobby lobby) { super("cLobbyLeave", lobby); }

    @Override
    public void execute(ServerController net, Connection connection){
        if (data instanceof ArrayList) {
            List<Object> receivedData = (ArrayList<Object>) data;

            lobbyId = (int) receivedData.get(0);
            playerId = (int) receivedData.get(1);

            Lobby lobby = net.getLobby(lobbyId);

            System.out.printf("Request from Player w. ID: %d to leave Lobby w. ID: %d \n", playerId, lobbyId);

            for (Connection c : net.getConnections(lobby)) {
                c.sendTCP(this);
            }

            net.leaveLobby(playerId, lobbyId);
            net.getPlayer(playerId).setIsPlayerReady(false);
        }
    }
}
