package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;
import com.mygdx.game.model.Player;

public class cPlayerJoin extends Command {

    public cPlayerJoin() {
        super("cPlayerJoin");
    }

    @Override
    public void execute(ServerController net, Connection connection){
        if (data instanceof Integer){
            int indexAvatar = (int) data;
            data = net.playerJoin(connection.getID(), indexAvatar);
            System.out.printf("Request to join from Client. Returning ID: %d \n", ((Player)data).getID());
            connection.sendTCP(this);
            if(((Player)data).getID() == -1) connection.close();
        }
    }
}
