package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;
import com.mygdx.game.model.Player;

public class cSetIndexAvatar extends Command {

    public cSetIndexAvatar() { super("cLobbyJoin"); }

    public cSetIndexAvatar(int indexAvatar) {
        super("cSetIndexAvatar", indexAvatar);
    }

    @Override
    public void execute(ServerController net, Connection connection){
        if(data instanceof Integer){
            int indexAvatar = (int) data;
            if (indexAvatar == -1) System.out.println("Failed to set index avatar.");
            else {
                for (Player p : net.getPlayers()) {
                    if(p.getID() == connection.getID()){
                        p.setIndexAvatar(indexAvatar);
                        connection.sendTCP(this);
                    }
                    else{
                        connection.sendTCP(new cSetIndexAvatar(-1));
                    }
                }
            }
        }
    }
}