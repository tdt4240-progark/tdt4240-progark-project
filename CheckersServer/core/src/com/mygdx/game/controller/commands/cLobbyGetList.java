package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;
import com.mygdx.game.model.Lobby;

import java.util.ArrayList;
import java.util.List;

public class cLobbyGetList extends Command{

    public cLobbyGetList(){ super("cLobbyGetList"); }

    @Override
    public void execute(ServerController net, Connection connection){
        data = net.getLobbies();

        // Exclude lobbies where game has already started
        List<Lobby> startedLobbies = new ArrayList<>();
        for (Lobby lobby : (List<Lobby>) data) {
            if (lobby.getLobbyGameStarted()) {
                startedLobbies.add(lobby);
            }
        }
        ((List<Lobby>) data).removeAll(startedLobbies);

        System.out.printf("Request to get list of lobbies from Client. Returning: %s \n", data.toString());
        connection.sendTCP(this);
        System.out.println("Number of available lobbies" + net.getLobbies().size());
    }
}
