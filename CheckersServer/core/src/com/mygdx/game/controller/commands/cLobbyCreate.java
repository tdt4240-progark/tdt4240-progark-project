package com.mygdx.game.controller.commands;

import com.esotericsoftware.kryonet.Connection;
import com.mygdx.game.controller.ServerController;
import com.mygdx.game.model.Lobby;

import java.util.ArrayList;

public class cLobbyCreate extends Command {

    public cLobbyCreate() { super("cLobbyCreate"); }

    //public cLobbyCreate(int MAX_PLAYERS) { super("cLobbyCreate", (Integer) MAX_PLAYERS); }

    @Override
    public void execute(ServerController net, Connection connection){
        if(data instanceof ArrayList){
            String name = (String) ((ArrayList) data).get(0);
            int MAX_PLAYERS = (int) ((ArrayList) data).get(1);
            data = net.createLobby(name, MAX_PLAYERS);
            System.out.printf("Request to create lobby received. Returning Lobby w. ID: %d \n", ((Lobby)data).getID());
            connection.sendTCP(this);
        }
    }
}
