package com.mygdx.game.model;

public class Player {

    private int ID;
    private int indexAvatar;
    private String playerName;
    private boolean isPlayerReady;

    public Player(){}

    public Player(int ID){ this.ID = ID; }

    public Player(int ID, int indexAvatar){
        this.ID = ID;
        this.indexAvatar = indexAvatar;
    }

    public int getID(){ return ID; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return ID == player.ID;
    }

    @Override
    public String toString() {
        return "Player{" +
                "ID=" + ID +
                '}';
    }

    public void setIndexAvatar(int indexAvatar){
        this.indexAvatar = indexAvatar;
    }

    public int getPlayerAvatar(int indexAvatar){
        return this.indexAvatar;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName(){
        return this.playerName;
    }

    public void setIsPlayerReady(boolean isPlayerReady) {
        this.isPlayerReady = isPlayerReady;
    }

    public boolean getIsPlayerReady(){
        return this.isPlayerReady;
    }
}
