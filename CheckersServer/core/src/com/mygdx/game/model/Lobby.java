package com.mygdx.game.model;

import java.util.ArrayList;
import java.util.HashSet;

public class Lobby {

    private int ID;
    private String name;
    private int MAX_PLAYERS;
    private ArrayList<Player> players;
    private boolean lobbyGameStarted = false;

    public Lobby() {}

    public Lobby(int ID){
        this.ID = ID;
        players = new ArrayList<Player>();
    }

    public Lobby(int ID, String name, int MAX_PLAYERS){
        this.ID = ID;
        this.name = name;
        this.MAX_PLAYERS = MAX_PLAYERS;
        players = new ArrayList<Player>();
    }

    public int playerJoin(Player player){
        if(players.size() < MAX_PLAYERS && !getPlayersID().contains(player.getID())){
            players.add(player);
            return 0;
        }
        return -1;
    }

    public int playerKick(Player player){
        if (players.contains(player)) {
            players.remove(player);
            return  0;
        }
        return -1;
    }

    public int getID(){ return ID; }

    public HashSet<Integer> getPlayersID(){
        HashSet set = new HashSet();
        for(Player player : players){
            set.add((Integer) player.getID());
        }
        return set;
    }

    public Player getPlayerByID(int playerID){

        Player returnPlayer = null;

        for (Player player : players){
            if (player.getID() == playerID) returnPlayer = player;
        }

        return returnPlayer;
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }

    public int getPlayersCount(){ return players.size(); }

    public boolean isFullAndReady(){
        if(MAX_PLAYERS == getPlayersCount()){
            for(Player player : players){
                if(!player.getIsPlayerReady()) {
                    System.out.println("This should run only once.");
                    return false;
                }
            }
            System.out.println("This should run a few times.");
            return true;
        }
        else{
            System.out.println("This should many times.");
            return false;
        }
    }

    public void setLobbyGameStarted(boolean lobbyGameStarted) {
        this.lobbyGameStarted = lobbyGameStarted;
    }

    public Boolean getLobbyGameStarted() {
        return lobbyGameStarted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lobby lobby = (Lobby) o;
        return ID == lobby.ID;
    }

    @Override
    public String toString() {
        return "Lobby{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", MAX_PLAYERS=" + MAX_PLAYERS +
                ", players=" + players +
                '}';
    }
}
